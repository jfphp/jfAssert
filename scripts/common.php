<?php

/**
 * Código común a otros scripts.
 */

use jf\php\generator\Exception;
use jf\php\generator\Generator;
use jf\utils\php\Manual;

require_once __DIR__ . '/../../autoload.php';

/**
 * Genera los archivos.
 *
 * @param array  $generator Configuración del generador.
 * @param string $subdir    Subdirectorio donde se crearán los archivos.
 * @param bool   $debug     Activa el modo depuración.
 *
 * @return void
 *
 * @throws Exception
 */
function generate(array $generator, string $subdir, bool $debug = TRUE) : void
{
    $outdir = dirname(__DIR__) . '/src/' . $subdir;
    foreach ([ 'classes', 'interfaces', 'traits' ] as $_type)
    {
        if (!empty($generator[ $_type ]))
        {
            foreach ($generator[ $_type ] as $_name => $_config)
            {
                $_config['name'] = $_name;
                $_generator      = Generator::new([ 'debug' => $debug, 'overwrite' => !$debug ]);
                $_generator->addClass($_config);
                $_generator->save("$outdir/$_name.php");
            }
        }
    }
}

/**
 * Devuelve una instancia del gestor del manual de PHP.
 *
 * @return Manual
 */
function getManual() : Manual
{
    return new Manual([ 'marks' => [] ]);
}

/**
 * Devuelve los traits del paquete.
 *
 * @return class-string[]
 */
function getTraits() : array
{
    return array_map(
        fn($file) => '\jf\assert\\' . basename($file, '.php'),
        array_filter(
            glob(__DIR__ . '/../src/T*.php'),
            fn($file) => preg_match('/^T[A-Z]/', basename($file))
        )
    );
}
