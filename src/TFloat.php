<?php

namespace jf\assert;

/**
 * Trait for comparisons between floats.
 */
trait TFloat
{
    /**
     * Check that a value of type float is in the given range.
     *
     * @param float  $value    Current value to compare.
     * @param float  $minValue Minimum value to compare.
     * @param float  $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function floatBetweenAnd(float $value, float $minValue, float $maxValue, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isTrue($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Check that a value of type float is greater than or equal to another.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function floatGreaterOrEqual(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isTrue($value >= $float, $message, ...$args);
    }

    /**
     * Check that a value of type float is greater than another.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function floatGreaterThan(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isTrue($value > $float, $message, ...$args);
    }

    /**
     * Check that a value of type float is identical (`===`) to another.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function floatIdentical(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isTrue($value === $float, $message, ...$args);
    }

    /**
     * Check that a value of type float is less than or equal to another.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function floatLessOrEqual(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isTrue($value <= $float, $message, ...$args);
    }

    /**
     * Check that a value of type float is less than another.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function floatLessThan(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isTrue($value < $float, $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::floatBetweenAnd`.
     *
     * @param float  $value    Current value to compare.
     * @param float  $minValue Minimum value to compare.
     * @param float  $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::floatBetweenAnd()
     */
    public static function notFloatBetweenAnd(float $value, float $minValue, float $maxValue, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isFalse($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::floatGreaterOrEqual`.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::floatGreaterOrEqual()
     */
    public static function notFloatGreaterOrEqual(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isFalse($value >= $float, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::floatGreaterThan`.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::floatGreaterThan()
     */
    public static function notFloatGreaterThan(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isFalse($value > $float, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::floatIdentical`.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::floatIdentical()
     */
    public static function notFloatIdentical(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isFalse($value === $float, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::floatLessOrEqual`.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::floatLessOrEqual()
     */
    public static function notFloatLessOrEqual(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isFalse($value <= $float, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::floatLessThan`.
     *
     * @param float  $value   Current value to compare.
     * @param float  $float   Float to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::floatLessThan()
     */
    public static function notFloatLessThan(float $value, float $float, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $float;

        static::isFalse($value < $float, $message, ...$args);
    }
}