<?php

namespace jf\assert;

/**
 * Trait that includes all traits in the package.
 */
trait TAll
{
    use TArray;
    use TAssert;
    use TClassObj;
    use TCtype;
    use TFileSystem;
    use TFilter;
    use TFloat;
    use TFuncHand;
    use TInfo;
    use TInteger;
    use TLogical;
    use TMath;
    use TMessage;
    use TStrings;
    use TVar;
}