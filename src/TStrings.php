<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `strings`.
 *
 * @see https://www.php.net/manual/en/book.strings.php
 */
trait TStrings
{
    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::regex`.
     *
     * @param string $haystack The string to search in.
     * @param string $pattern  The pattern to search for.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::regex()
     */
    public static function notRegex(string $haystack, string $pattern, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $pattern;

        static::isFalse(preg_match($pattern, $haystack) > 0, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strContains`.
     *
     * @param string $haystack The string to search in.
     * @param string $needle   The substring to search for in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strContains()
     */
    public static function notStrContains(string $haystack, string $needle, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $needle;

        static::isFalse(str_contains($haystack, $needle), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strEndsWith`.
     *
     * @param string $haystack The string to search in.
     * @param string $needle   The substring to search for in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strEndsWith()
     */
    public static function notStrEndsWith(string $haystack, string $needle, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $needle;

        static::isFalse(str_ends_with($haystack, $needle), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strLengthBetweenAnd`.
     *
     * @param string $value    String to validate.
     * @param int    $minValue Minimum value to compare.
     * @param int    $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strLengthBetweenAnd()
     */
    public static function notStrLengthBetweenAnd(string $value, int $minValue, int $maxValue, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isFalse($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strLengthGreaterOrEqual`.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strLengthGreaterOrEqual()
     */
    public static function notStrLengthGreaterOrEqual(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isFalse($value >= $length, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strLengthGreaterThan`.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strLengthGreaterThan()
     */
    public static function notStrLengthGreaterThan(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isFalse($value > $length, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strLengthIdentical`.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strLengthIdentical()
     */
    public static function notStrLengthIdentical(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isFalse($value === $length, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strLengthLessOrEqual`.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strLengthLessOrEqual()
     */
    public static function notStrLengthLessOrEqual(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isFalse($value <= $length, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strLengthLessThan`.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strLengthLessThan()
     */
    public static function notStrLengthLessThan(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isFalse($value < $length, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::strStartsWith`.
     *
     * @param string $haystack The string to search in.
     * @param string $needle   The substring to search for in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::strStartsWith()
     */
    public static function notStrStartsWith(string $haystack, string $needle, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $needle;

        static::isFalse(str_starts_with($haystack, $needle), $message, ...$args);
    }

    /**
     * Check that a string matches a regular expression.
     *
     * @param string $haystack The string to search in.
     * @param string $pattern  The pattern to search for.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function regex(string $haystack, string $pattern, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $pattern;

        static::isTrue(preg_match($pattern, $haystack) > 0, $message, ...$args);
    }

    /**
     * Determine if a string contains a given substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle   The substring to search for in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strContains(string $haystack, string $needle, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $needle;

        static::isTrue(str_contains($haystack, $needle), $message, ...$args);
    }

    /**
     * Checks if a string ends with a given substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle   The substring to search for in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strEndsWith(string $haystack, string $needle, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $needle;

        static::isTrue(str_ends_with($haystack, $needle), $message, ...$args);
    }

    /**
     * Check that the length of string is in the given range.
     *
     * @param string $value    String to validate.
     * @param int    $minValue Minimum value to compare.
     * @param int    $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strLengthBetweenAnd(string $value, int $minValue, int $maxValue, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isTrue($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Check that the length of string is greater than or equal to another.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strLengthGreaterOrEqual(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isTrue($value >= $length, $message, ...$args);
    }

    /**
     * Check that the length of string is greater than another.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strLengthGreaterThan(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isTrue($value > $length, $message, ...$args);
    }

    /**
     * Check that the length of string is identical (`===`) to another.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strLengthIdentical(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isTrue($value === $length, $message, ...$args);
    }

    /**
     * Check that the length of string is less than or equal to another.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strLengthLessOrEqual(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isTrue($value <= $length, $message, ...$args);
    }

    /**
     * Check that the length of string is less than another.
     *
     * @param string $value   String to validate.
     * @param int    $length  Length to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strLengthLessThan(string $value, int $length, string $message = '', mixed ...$args) : void
    {
        $value = strlen($value);

        $args[] = $value;
        $args[] = $length;

        static::isTrue($value < $length, $message, ...$args);
    }

    /**
     * Checks if a string starts with a given substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle   The substring to search for in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function strStartsWith(string $haystack, string $needle, string $message = '', mixed ...$args) : void
    {
        $args[] = $haystack;
        $args[] = $needle;

        static::isTrue(str_starts_with($haystack, $needle), $message, ...$args);
    }
}