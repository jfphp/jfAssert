<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `filter`.
 *
 * @see https://www.php.net/manual/en/book.filter.php
 */
trait TFilter
{
    /**
     * Checks if variable of specified type exists.
     *
     * @param int    $inputType One of `INPUT_GET`, `INPUT_POST`, `INPUT_COOKIE`, `INPUT_SERVER`, or `INPUT_ENV`.
     * @param string $varName   Name of a variable to check.
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterHasVar(int $inputType, string $varName, string $message = '', mixed ...$args) : void
    {
        $args[] = $inputType;
        $args[] = $varName;

        static::isTrue(filter_has_var($inputType, $varName), $message, ...$args);
    }

    /**
     * Returns true for `1`, `true`, `on` and `yes`. Returns false otherwise.
     *
     * If `FILTER_NULL_ON_FAILURE` is set, false is returned only for `0`, `false`, `off`, `no`,
     * and ``, and null is returned for all non-boolean values.
     *
     * String values are trimmed using `trim()` before comparison.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateBool(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_BOOL, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates whether the domain name label lengths are valid.
     *
     * Validates domain names against `RFC1034`, `RFC1035`, `RFC952`, `RFC1123`, `RFC2732`,
     * `RFC2181`, and `RFC1123`.
     *
     * Optional flag `FILTER_FLAG_HOSTNAME` adds ability to specifically validate hostnames (they
     * must start with an alphanumeric character and contain only alphanumerics or hyphens).
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_HOSTNAME` | `FILTER_NULL_ON_FAILURE`
     *                        ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateDomain(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_DOMAIN, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates whether the value is a valid e-mail address.
     *
     * In general, this validates e-mail addresses against the addr-spec syntax in `RFC822`, with
     * the  exceptions that comments and whitespace folding and dotless domain names are not
     * supported.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_EMAIL_UNICODE` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateEmail(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_EMAIL, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates value as float, optionally from the specified range, and converts to float on
     * success.
     *
     * String values are trimmed using `trim()` before comparison.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_ALLOW_THOUSAND` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateFloat(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_FLOAT, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates value as integer, optionally from the specified range, and converts to int on
     * success.
     *
     * String values are trimmed using `trim()` before comparison.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_ALLOW_OCTAL` |
     *                        `FILTER_FLAG_ALLOW_HEX` | `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateInt(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_INT, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates value as IP address, optionally only IPv4 or IPv6 or not from private or
     * reserved ranges.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_IPV4` | `FILTER_FLAG_IPV6` |
     *                        `FILTER_FLAG_NO_PRIV_RANGE` | `FILTER_FLAG_NO_RES_RANGE` | `FILTER_FLAG_GLOBAL_RANGE` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateIp(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_IP, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates value as MAC address.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateMac(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_MAC, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates value against regexp, a Perl-compatible regular expression.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateRegexp(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_REGEXP, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Validates value as URL (according to http://www.faqs.org/rfcs/rfc2396), optionally with
     * required components.
     *
     * Beware a valid URL may not specify the HTTP protocol `http://` so further validation may
     * be required to determine the URL uses an expected protocol, e.g. `ssh://` or `mailto:`. 
     *
     * Note that the function will only find ASCII URLs to be valid; internationalized domain
     * names (containing non-ASCII characters) will fail.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_SCHEME_REQUIRED` |
     *                        `FILTER_FLAG_HOST_REQUIRED` | `FILTER_FLAG_PATH_REQUIRED` | `FILTER_FLAG_QUERY_REQUIRED` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterValidateUrl(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, FILTER_VALIDATE_URL, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Filters a variable with a specified filter.
     *
     * @param mixed     $value   Value to filter. Note that scalar values are `converted to string` internally before they
     *                           are filtered.
     * @param int       $filter  The ID of the filter to apply. The `Types of filters` manual page lists the available
     *                           filters. If omitted, `FILTER_DEFAULT` will be used, which is equivalent to
     *                           ``FILTER_UNSAFE_RAW``. This will result in no filtering taking place by default.
     * @param array|int $options Associative array of options or bitwise disjunction of flags. If filter accepts options,
     *                           flags can be provided in "flags" field of array. For the "callback" filter, `callable`
     *                           type should be passed. The callback must accept one argument, the value to be filtered,
     *                           and return the value after filtering/sanitizing it.
     * @param string    $message Message of the exception.
     * @param mixed     ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function filterVar(mixed $value, int $filter = FILTER_DEFAULT, array|int $options = 0, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $filter;
        $args[] = $options;

        static::isTrue((bool) filter_var($value, $filter, $options), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::filterHasVar`.
     *
     * @param int    $inputType One of `INPUT_GET`, `INPUT_POST`, `INPUT_COOKIE`, `INPUT_SERVER`, or `INPUT_ENV`.
     * @param string $varName   Name of a variable to check.
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterHasVar()
     */
    public static function notFilterHasVar(int $inputType, string $varName, string $message = '', mixed ...$args) : void
    {
        $args[] = $inputType;
        $args[] = $varName;

        static::isFalse(filter_has_var($inputType, $varName), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateBool`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateBool()
     */
    public static function notFilterValidateBool(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_BOOL, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateDomain`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_HOSTNAME` | `FILTER_NULL_ON_FAILURE`
     *                        ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateDomain()
     */
    public static function notFilterValidateDomain(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_DOMAIN, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateEmail`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_EMAIL_UNICODE` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateEmail()
     */
    public static function notFilterValidateEmail(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_EMAIL, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateFloat`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_ALLOW_THOUSAND` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateFloat()
     */
    public static function notFilterValidateFloat(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_FLOAT, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateInt`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_ALLOW_OCTAL` |
     *                        `FILTER_FLAG_ALLOW_HEX` | `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateInt()
     */
    public static function notFilterValidateInt(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_INT, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateIp`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_IPV4` | `FILTER_FLAG_IPV6` |
     *                        `FILTER_FLAG_NO_PRIV_RANGE` | `FILTER_FLAG_NO_RES_RANGE` | `FILTER_FLAG_GLOBAL_RANGE` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateIp()
     */
    public static function notFilterValidateIp(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_IP, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateMac`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateMac()
     */
    public static function notFilterValidateMac(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_MAC, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateRegexp`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateRegexp()
     */
    public static function notFilterValidateRegexp(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_REGEXP, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterValidateUrl`.
     *
     * @param mixed  $value   Value to validate.
     * @param int    $flags   Bitwise disjunction of allowed flags ([ `FILTER_FLAG_SCHEME_REQUIRED` |
     *                        `FILTER_FLAG_HOST_REQUIRED` | `FILTER_FLAG_PATH_REQUIRED` | `FILTER_FLAG_QUERY_REQUIRED` |
     *                        `FILTER_NULL_ON_FAILURE` ]).
     * @param array  $options Associative array of options.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterValidateUrl()
     */
    public static function notFilterValidateUrl(mixed $value, int $flags = 0, array $options = [], string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $flags;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, FILTER_VALIDATE_URL, [ 'flags' => $flags, 'options' => $options ]), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::filterVar`.
     *
     * @param mixed     $value   Value to filter. Note that scalar values are `converted to string` internally before they
     *                           are filtered.
     * @param int       $filter  The ID of the filter to apply. The `Types of filters` manual page lists the available
     *                           filters. If omitted, `FILTER_DEFAULT` will be used, which is equivalent to
     *                           ``FILTER_UNSAFE_RAW``. This will result in no filtering taking place by default.
     * @param array|int $options Associative array of options or bitwise disjunction of flags. If filter accepts options,
     *                           flags can be provided in "flags" field of array. For the "callback" filter, `callable`
     *                           type should be passed. The callback must accept one argument, the value to be filtered,
     *                           and return the value after filtering/sanitizing it.
     * @param string    $message Message of the exception.
     * @param mixed     ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::filterVar()
     */
    public static function notFilterVar(mixed $value, int $filter = FILTER_DEFAULT, array|int $options = 0, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $filter;
        $args[] = $options;

        static::isFalse((bool) filter_var($value, $filter, $options), $message, ...$args);
    }
}