<?php

namespace jf\assert;

use Exception;
use Throwable;

/**
 * Class used to do validations and throw exceptions if they are not met.
 */
class Assert extends Exception
{
    use TAll;

    /**
     * Default exception code.
     *
     * @var int
     */
    public const CODE = -1;

    /**
     * @inheritdoc
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = NULL)
    {
        parent::__construct($message, $code ?: static::CODE, $previous);
    }
}