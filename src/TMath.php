<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `math`.
 *
 * @see https://www.php.net/manual/en/book.math.php
 */
trait TMath
{
    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Finds whether a value is a legal finite number.
     *
     * @param float  $num     The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isFinite(float $num, string $message = '', mixed ...$args) : void
    {
        $args[] = $num;

        static::isTrue(is_finite($num), $message, ...$args);
    }

    /**
     * Finds whether a value is infinite.
     *
     * @param float  $num     The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isInfinite(float $num, string $message = '', mixed ...$args) : void
    {
        $args[] = $num;

        static::isTrue(is_infinite($num), $message, ...$args);
    }

    /**
     * Finds whether a value is not a number.
     *
     * @param float  $num     The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isNan(float $num, string $message = '', mixed ...$args) : void
    {
        $args[] = $num;

        static::isTrue(is_nan($num), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::isFinite`.
     *
     * @param float  $num     The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isFinite()
     */
    public static function notIsFinite(float $num, string $message = '', mixed ...$args) : void
    {
        $args[] = $num;

        static::isFalse(is_finite($num), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isInfinite`.
     *
     * @param float  $num     The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isInfinite()
     */
    public static function notIsInfinite(float $num, string $message = '', mixed ...$args) : void
    {
        $args[] = $num;

        static::isFalse(is_infinite($num), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isNan`.
     *
     * @param float  $num     The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isNan()
     */
    public static function notIsNan(float $num, string $message = '', mixed ...$args) : void
    {
        $args[] = $num;

        static::isFalse(is_nan($num), $message, ...$args);
    }
}