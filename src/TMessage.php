<?php

namespace jf\assert;

use jf\assert\dict\Messages;
use Stringable;

/**
 * Trait for building the error messages.
 *
 * @mixin Stringable
 */
trait TMessage
{
    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return static::class;
    }

    /**
     * Returns class name without namespace.
     *
     * @return string
     */
    public static function classname() : string
    {
        $_class = static::class;

        return str_contains($_class, '\\')
            ? substr($_class, strrpos($_class, '\\') + 1)
            : $_class;
    }

    /**
     * Transforms text in `camelCase` format to `kebab-case`.
     *
     * @param string $text Texo to transform.
     *
     * @return string
     */
    public static function dasherize(string $text) : string
    {
        return strtolower(preg_replace('/([^A-Z])([A-Z])/u', '$1-$2', $text));
    }

    /**
     * Translates the error message of the function that threw the exception.
     *
     * @param string $fn Name of the function.
     *
     * @return string
     */
    public static function getMessageForFunction(string $fn) : string
    {
        return Messages::get($fn) ?? static::dasherize(static::classname()) . '/' . static::dasherize($fn);
    }
}