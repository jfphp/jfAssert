<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `405` (`Method Not Allowed`).
 */
class MethodNotAllowed extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 405;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Method Not Allowed';
}
