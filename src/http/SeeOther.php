<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `303` (`See Other`).
 */
class SeeOther extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 303;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'See Other';
}
