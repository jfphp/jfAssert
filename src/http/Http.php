<?php

namespace jf\assert\http;

use jf\assert\Assert;
use Throwable;

/**
 * Class base for HTTP exceptions.
 */
class Http extends Assert
{
    /**
     * Default message of the exception.
     *
     * @var string
     */
    public const MESSAGE = 'HTTP Error';

    /**
     * @inheritdoc
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = NULL)
    {
        parent::__construct($message ?: static::MESSAGE, $code ?: static::CODE, $previous);
    }

    /**
     * Builds an instance depending of HTTP error code specified.
     *
     * @param int    $code    HTTP error code.
     * @param string $message Message of the exception.
     *
     * @return static
     */
    public static function fromCode(int $code, string $message = '') : static
    {
        return match ($code)
        {
            300     => new MultipleChoices($message, $code),
            301     => new MovedPermanently($message, $code),
            302     => new Found($message, $code),
            303     => new SeeOther($message, $code),
            304     => new NotModified($message, $code),
            305     => new UseProxy($message, $code),
            306     => new Reserved($message, $code),
            307     => new TemporaryRedirect($message, $code),
            308     => new PermanentRedirect($message, $code),
            400     => new BadRequest($message, $code),
            401     => new Unauthorized($message, $code),
            402     => new PaymentRequired($message, $code),
            403     => new Forbidden($message, $code),
            404     => new NotFound($message, $code),
            405     => new MethodNotAllowed($message, $code),
            406     => new NotAcceptable($message, $code),
            407     => new ProxyAuthenticationRequired($message, $code),
            408     => new RequestTimeout($message, $code),
            409     => new Conflict($message, $code),
            410     => new Gone($message, $code),
            411     => new LengthRequired($message, $code),
            412     => new PreconditionFailed($message, $code),
            413     => new PayloadTooLarge($message, $code),
            414     => new UriTooLong($message, $code),
            415     => new UnsupportedMediaType($message, $code),
            416     => new RangeNotSatisfiable($message, $code),
            417     => new ExpectationFailed($message, $code),
            418     => new ImATeapot($message, $code),
            421     => new MisdirectedRequest($message, $code),
            422     => new UnprocessableEntity($message, $code),
            423     => new Locked($message, $code),
            424     => new FailedDependency($message, $code),
            425     => new TooEarly($message, $code),
            426     => new UpgradeRequired($message, $code),
            428     => new PreconditionRequired($message, $code),
            429     => new TooManyRequests($message, $code),
            431     => new RequestHeaderFieldsTooLarge($message, $code),
            451     => new UnavailableForLegalReasons($message, $code),
            500     => new InternalServerError($message, $code),
            501     => new NotImplemented($message, $code),
            502     => new BadGateway($message, $code),
            503     => new ServiceUnavailable($message, $code),
            504     => new GatewayTimeout($message, $code),
            505     => new VersionNotSupported($message, $code),
            506     => new VariantAlsoNegotiates($message, $code),
            507     => new InsufficientStorage($message, $code),
            508     => new LoopDetected($message, $code),
            510     => new NotExtended($message, $code),
            511     => new NetworkAuthenticationRequired($message, $code),
            default => new static($message, $code)
        };
    }
}
