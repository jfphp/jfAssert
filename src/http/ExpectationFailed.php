<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `417` (`Expectation Failed`).
 */
class ExpectationFailed extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 417;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Expectation Failed';
}
