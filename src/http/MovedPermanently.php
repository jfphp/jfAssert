<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `301` (`Moved Permanently`).
 */
class MovedPermanently extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 301;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Moved Permanently';
}
