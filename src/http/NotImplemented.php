<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `501` (`Not Implemented`).
 */
class NotImplemented extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 501;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Not Implemented';
}
