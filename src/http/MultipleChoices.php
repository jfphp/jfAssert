<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `300` (`Multiple Choices`).
 */
class MultipleChoices extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 300;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Multiple Choices';
}
