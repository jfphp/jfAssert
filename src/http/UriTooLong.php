<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `414` (`Uri Too Long`).
 */
class UriTooLong extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 414;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Uri Too Long';
}
