<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `416` (`Range Not Satisfiable`).
 */
class RangeNotSatisfiable extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 416;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Range Not Satisfiable';
}
