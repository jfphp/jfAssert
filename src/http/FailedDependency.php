<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `424` (`Failed Dependency`).
 */
class FailedDependency extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 424;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Failed Dependency';
}
