<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `507` (`Insufficient Storage`).
 */
class InsufficientStorage extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 507;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Insufficient Storage';
}
