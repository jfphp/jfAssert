<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `406` (`Not Acceptable`).
 */
class NotAcceptable extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 406;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Not Acceptable';
}
