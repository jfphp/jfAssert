<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `502` (`Bad Gateway`).
 */
class BadGateway extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 502;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Bad Gateway';
}
