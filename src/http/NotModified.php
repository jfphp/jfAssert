<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `304` (`Not Modified`).
 */
class NotModified extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 304;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Not Modified';
}
