<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `402` (`Payment Required`).
 */
class PaymentRequired extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 402;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Payment Required';
}
