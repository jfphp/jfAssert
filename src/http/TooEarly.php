<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `425` (`Too Early`).
 */
class TooEarly extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 425;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Too Early';
}
