<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `423` (`Locked`).
 */
class Locked extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 423;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Locked';
}
