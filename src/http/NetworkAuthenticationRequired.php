<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `511` (`Network Authentication Required`).
 */
class NetworkAuthenticationRequired extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 511;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Network Authentication Required';
}
