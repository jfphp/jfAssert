<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `415` (`Unsupported Media Type`).
 */
class UnsupportedMediaType extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 415;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Unsupported Media Type';
}
