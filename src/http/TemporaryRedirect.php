<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `307` (`Temporary Redirect`).
 */
class TemporaryRedirect extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 307;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Temporary Redirect';
}
