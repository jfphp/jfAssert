<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `426` (`Upgrade Required`).
 */
class UpgradeRequired extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 426;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Upgrade Required';
}
