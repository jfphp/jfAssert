<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `505` (`Version Not Supported`).
 */
class VersionNotSupported extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 505;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Version Not Supported';
}
