<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `431` (`Request Header Fields Too Large`).
 */
class RequestHeaderFieldsTooLarge extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 431;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Request Header Fields Too Large';
}
