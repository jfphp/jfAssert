<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `510` (`Not Extended`).
 */
class NotExtended extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 510;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Not Extended';
}
