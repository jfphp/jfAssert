<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `418` (`Im A Teapot`).
 */
class ImATeapot extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 418;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Im A Teapot';
}
