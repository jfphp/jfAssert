<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `401` (`Unauthorized`).
 */
class Unauthorized extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 401;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Unauthorized';
}
