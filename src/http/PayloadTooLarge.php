<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `413` (`Payload Too Large`).
 */
class PayloadTooLarge extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 413;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Payload Too Large';
}
