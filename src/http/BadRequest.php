<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `400` (`Bad Request`).
 */
class BadRequest extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 400;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Bad Request';
}
