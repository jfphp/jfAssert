<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `409` (`Conflict`).
 */
class Conflict extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 409;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Conflict';
}
