<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `302` (`Found`).
 */
class Found extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 302;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Found';
}
