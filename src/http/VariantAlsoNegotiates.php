<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `506` (`Variant Also Negotiates`).
 */
class VariantAlsoNegotiates extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 506;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Variant Also Negotiates';
}
