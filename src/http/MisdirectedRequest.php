<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `421` (`Misdirected Request`).
 */
class MisdirectedRequest extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 421;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Misdirected Request';
}
