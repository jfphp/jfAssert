<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `500` (`Internal Server Error`).
 */
class InternalServerError extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 500;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Internal Server Error';
}
