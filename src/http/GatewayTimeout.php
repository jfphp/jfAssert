<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `504` (`Gateway Timeout`).
 */
class GatewayTimeout extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 504;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Gateway Timeout';
}
