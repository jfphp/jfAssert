<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `404` (`Not Found`).
 */
class NotFound extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 404;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Not Found';
}
