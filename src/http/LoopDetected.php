<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `508` (`Loop Detected`).
 */
class LoopDetected extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 508;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Loop Detected';
}
