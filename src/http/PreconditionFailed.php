<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `412` (`Precondition Failed`).
 */
class PreconditionFailed extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 412;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Precondition Failed';
}
