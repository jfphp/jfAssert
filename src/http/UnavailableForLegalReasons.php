<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `451` (`Unavailable For Legal Reasons`).
 */
class UnavailableForLegalReasons extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 451;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Unavailable For Legal Reasons';
}
