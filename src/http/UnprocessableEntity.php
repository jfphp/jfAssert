<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `422` (`Unprocessable Entity`).
 */
class UnprocessableEntity extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 422;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Unprocessable Entity';
}
