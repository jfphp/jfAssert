<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `410` (`Gone`).
 */
class Gone extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 410;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Gone';
}
