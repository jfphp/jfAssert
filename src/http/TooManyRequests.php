<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `429` (`Too Many Requests`).
 */
class TooManyRequests extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 429;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Too Many Requests';
}
