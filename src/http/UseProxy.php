<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `305` (`Use Proxy`).
 */
class UseProxy extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 305;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Use Proxy';
}
