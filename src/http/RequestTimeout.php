<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `408` (`Request Timeout`).
 */
class RequestTimeout extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 408;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Request Timeout';
}
