<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `411` (`Length Required`).
 */
class LengthRequired extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 411;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Length Required';
}
