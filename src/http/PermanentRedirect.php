<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `308` (`Permanent Redirect`).
 */
class PermanentRedirect extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 308;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Permanent Redirect';
}
