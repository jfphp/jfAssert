<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `407` (`Proxy Authentication Required`).
 */
class ProxyAuthenticationRequired extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 407;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Proxy Authentication Required';
}
