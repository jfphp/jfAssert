<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `503` (`Service Unavailable`).
 */
class ServiceUnavailable extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 503;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Service Unavailable';
}
