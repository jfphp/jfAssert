<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `403` (`Forbidden`).
 */
class Forbidden extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 403;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Forbidden';
}
