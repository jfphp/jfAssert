<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `428` (`Precondition Required`).
 */
class PreconditionRequired extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 428;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Precondition Required';
}
