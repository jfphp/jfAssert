<?php

namespace jf\assert\http;

/**
 * Assertions that return the HTTP status code `306` (`Reserved`).
 */
class Reserved extends Http
{
    /**
     * @inheritdoc
     */
    public const CODE = 306;

    /**
     * @inheritdoc
     */
    public const MESSAGE = 'Reserved';
}
