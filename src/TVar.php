<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `var`.
 *
 * @see https://www.php.net/manual/en/book.var.php
 */
trait TVar
{
    /**
     * Finds whether a variable is an array.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isArray(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_array($value), $message, ...$args);
    }

    /**
     * Finds out whether a variable is a boolean.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isBool(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_bool($value), $message, ...$args);
    }

    /**
     * Verify that a value can be called as a function from the current scope.
     *
     * @param mixed       $value        The value to check.
     * @param bool        $syntaxOnly   If set to `true` the function only verifies that `value` might be a function or method. It
     *                                  will only reject simple variables that are not strings, or an array that does not have a
     *                                  valid structure to be used as a callback. The valid ones are supposed to have only 2
     *                                  entries, the first of which is an object or a string, and the second a string.
     * @param string|NULL $callableName Receives the "callable name". In the example below it is "someClass::someMethod". Note,
     *                                  however, that despite the implication that someClass::SomeMethod() is a callable static
     *                                  method, this is not the case.
     * @param string      $message      Message of the exception.
     * @param mixed       ...$args      Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isCallable(mixed $value, bool $syntaxOnly = FALSE, ?string &$callableName = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $syntaxOnly;
        $args[] = $callableName;

        static::isTrue(is_callable($value, $syntaxOnly, $callableName), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is a countable value.
     *
     * @param mixed  $value   The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isCountable(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_countable($value), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Finds whether the type of a variable is float.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isFloat(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_float($value), $message, ...$args);
    }

    /**
     * Check that a value is an instance of specified class.
     *
     * @param mixed  $value   Value to check.
     * @param string $class   Name of the expected class.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isInstanceOf(mixed $value, string $class, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $class;

        static::isTrue($value instanceof $class, $message, ...$args);
    }

    /**
     * Find whether the type of a variable is integer.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isInt(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_int($value), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is an iterable value.
     *
     * @param mixed  $value   The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isIterable(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_iterable($value), $message, ...$args);
    }

    /**
     * Finds whether a variable is null.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isNull(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_null($value), $message, ...$args);
    }

    /**
     * Finds whether a variable is a number or a numeric string.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isNumeric(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_numeric($value), $message, ...$args);
    }

    /**
     * Finds whether a variable is an object.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isObject(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_object($value), $message, ...$args);
    }

    /**
     * Finds whether a variable is a resource.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isResource(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_resource($value), $message, ...$args);
    }

    /**
     * Finds whether a variable is a scalar.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isScalar(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_scalar($value), $message, ...$args);
    }

    /**
     * Find whether the type of a variable is string.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isString(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_string($value), $message, ...$args);
    }

    /**
     * Check that a value is an array or an instance of `\Traversable`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isTraversable(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(is_array($value) || $value instanceof \Traversable, $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::isArray`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isArray()
     */
    public static function notIsArray(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_array($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isBool`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isBool()
     */
    public static function notIsBool(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_bool($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isCallable`.
     *
     * @param mixed       $value        The value to check.
     * @param bool        $syntaxOnly   If set to `true` the function only verifies that `value` might be a function or method. It
     *                                  will only reject simple variables that are not strings, or an array that does not have a
     *                                  valid structure to be used as a callback. The valid ones are supposed to have only 2
     *                                  entries, the first of which is an object or a string, and the second a string.
     * @param string|NULL $callableName Receives the "callable name". In the example below it is "someClass::someMethod". Note,
     *                                  however, that despite the implication that someClass::SomeMethod() is a callable static
     *                                  method, this is not the case.
     * @param string      $message      Message of the exception.
     * @param mixed       ...$args      Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isCallable()
     */
    public static function notIsCallable(mixed $value, bool $syntaxOnly = FALSE, ?string &$callableName = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $syntaxOnly;
        $args[] = $callableName;

        static::isFalse(is_callable($value, $syntaxOnly, $callableName), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isCountable`.
     *
     * @param mixed  $value   The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isCountable()
     */
    public static function notIsCountable(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_countable($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isFloat`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isFloat()
     */
    public static function notIsFloat(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_float($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isInstanceOf`.
     *
     * @param mixed  $value   Value to check.
     * @param string $class   Name of the expected class.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isInstanceOf()
     */
    public static function notIsInstanceOf(mixed $value, string $class, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $class;

        static::isFalse($value instanceof $class, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isInt`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isInt()
     */
    public static function notIsInt(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_int($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isIterable`.
     *
     * @param mixed  $value   The value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isIterable()
     */
    public static function notIsIterable(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_iterable($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isNull`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isNull()
     */
    public static function notIsNull(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_null($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isNumeric`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isNumeric()
     */
    public static function notIsNumeric(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_numeric($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isObject`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isObject()
     */
    public static function notIsObject(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_object($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isResource`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isResource()
     */
    public static function notIsResource(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_resource($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isScalar`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isScalar()
     */
    public static function notIsScalar(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_scalar($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isString`.
     *
     * @param mixed  $value   The variable being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isString()
     */
    public static function notIsString(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_string($value), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isTraversable`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isTraversable()
     */
    public static function notIsTraversable(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(is_array($value) || $value instanceof \Traversable, $message, ...$args);
    }
}