<?php

namespace jf\assert;

use Countable;

/**
 * Trait for assertions using functions from PHP module `array`.
 *
 * @see https://www.php.net/manual/en/book.array.php
 */
trait TArray
{
    /**
     * Check that the number of elements in an array or in a Countable object is in the given
     * range.
     *
     * @param Countable|array $value    Countable or array to validate.
     * @param int             $minValue Minimum value to compare.
     * @param int             $maxValue Maximum value to compare.
     * @param string          $message  Message of the exception.
     * @param mixed           ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayCountBetweenAnd(Countable|array $value, int $minValue, int $maxValue, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isTrue($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Check that the number of elements in an array or in a Countable object is greater than or
     * equal to another.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayCountGreaterOrEqual(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isTrue($value >= $count, $message, ...$args);
    }

    /**
     * Check that the number of elements in an array or in a Countable object is greater than
     * another.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayCountGreaterThan(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isTrue($value > $count, $message, ...$args);
    }

    /**
     * Check that the number of elements in an array or in a Countable object is identical
     * (`===`) to another.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayCountIdentical(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isTrue($value === $count, $message, ...$args);
    }

    /**
     * Check that the number of elements in an array or in a Countable object is less than or
     * equal to another.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayCountLessOrEqual(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isTrue($value <= $count, $message, ...$args);
    }

    /**
     * Check that the number of elements in an array or in a Countable object is less than
     * another.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayCountLessThan(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isTrue($value < $count, $message, ...$args);
    }

    /**
     * Checks whether a given array is a list.
     *
     * @param array  $array   The array being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayIsList(array $array, string $message = '', mixed ...$args) : void
    {
        $args[] = $array;

        static::isTrue(array_is_list($array), $message, ...$args);
    }

    /**
     * Checks if the given key or index exists in the array.
     *
     * @param int|string $key     Value to check.
     * @param array      $array   An array with keys to check.
     * @param string     $message Message of the exception.
     * @param mixed      ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function arrayKeyExists(int|string $key, array $array, string $message = '', mixed ...$args) : void
    {
        $args[] = $key;
        $args[] = $array;

        static::isTrue(array_key_exists($key, $array), $message, ...$args);
    }

    /**
     * Checks if a value exists in an array.
     *
     * @param mixed  $needle   The searched value. Note: If `needle` is a string, the comparison is done in a
     *                         case-sensitive manner.
     * @param array  $haystack The array.
     * @param bool   $strict   If the third parameter `strict` is set to `true` then the in_array() function will also
     *                         check the `types` of the `needle` in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function inArray(mixed $needle, array $haystack, bool $strict = FALSE, string $message = '', mixed ...$args) : void
    {
        $args[] = $needle;
        $args[] = $haystack;
        $args[] = $strict;

        static::isTrue(in_array($needle, $haystack, $strict), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::arrayCountBetweenAnd`.
     *
     * @param Countable|array $value    Countable or array to validate.
     * @param int             $minValue Minimum value to compare.
     * @param int             $maxValue Maximum value to compare.
     * @param string          $message  Message of the exception.
     * @param mixed           ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayCountBetweenAnd()
     */
    public static function notArrayCountBetweenAnd(Countable|array $value, int $minValue, int $maxValue, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isFalse($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayCountGreaterOrEqual`.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayCountGreaterOrEqual()
     */
    public static function notArrayCountGreaterOrEqual(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isFalse($value >= $count, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayCountGreaterThan`.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayCountGreaterThan()
     */
    public static function notArrayCountGreaterThan(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isFalse($value > $count, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayCountIdentical`.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayCountIdentical()
     */
    public static function notArrayCountIdentical(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isFalse($value === $count, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayCountLessOrEqual`.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayCountLessOrEqual()
     */
    public static function notArrayCountLessOrEqual(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isFalse($value <= $count, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayCountLessThan`.
     *
     * @param Countable|array $value   Countable or array to validate.
     * @param int             $count   Count to compare.
     * @param string          $message Message of the exception.
     * @param mixed           ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayCountLessThan()
     */
    public static function notArrayCountLessThan(Countable|array $value, int $count, string $message = '', mixed ...$args) : void
    {
        $value = count($value);

        $args[] = $value;
        $args[] = $count;

        static::isFalse($value < $count, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayIsList`.
     *
     * @param array  $array   The array being evaluated.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayIsList()
     */
    public static function notArrayIsList(array $array, string $message = '', mixed ...$args) : void
    {
        $args[] = $array;

        static::isFalse(array_is_list($array), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::arrayKeyExists`.
     *
     * @param int|string $key     Value to check.
     * @param array      $array   An array with keys to check.
     * @param string     $message Message of the exception.
     * @param mixed      ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::arrayKeyExists()
     */
    public static function notArrayKeyExists(int|string $key, array $array, string $message = '', mixed ...$args) : void
    {
        $args[] = $key;
        $args[] = $array;

        static::isFalse(array_key_exists($key, $array), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::inArray`.
     *
     * @param mixed  $needle   The searched value. Note: If `needle` is a string, the comparison is done in a
     *                         case-sensitive manner.
     * @param array  $haystack The array.
     * @param bool   $strict   If the third parameter `strict` is set to `true` then the in_array() function will also
     *                         check the `types` of the `needle` in the `haystack`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::inArray()
     */
    public static function notInArray(mixed $needle, array $haystack, bool $strict = FALSE, string $message = '', mixed ...$args) : void
    {
        $args[] = $needle;
        $args[] = $haystack;
        $args[] = $strict;

        static::isFalse(in_array($needle, $haystack, $strict), $message, ...$args);
    }
}