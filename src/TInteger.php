<?php

namespace jf\assert;

/**
 * Trait for comparisons between integers.
 */
trait TInteger
{
    /**
     * Check that a value of type int is in the given range.
     *
     * @param int    $value    Current value to compare.
     * @param int    $minValue Minimum value to compare.
     * @param int    $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function intBetweenAnd(int $value, int $minValue, int $maxValue, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isTrue($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Check that a value of type int is greater than or equal to another.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function intGreaterOrEqual(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isTrue($value >= $integer, $message, ...$args);
    }

    /**
     * Check that a value of type int is greater than another.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function intGreaterThan(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isTrue($value > $integer, $message, ...$args);
    }

    /**
     * Check that a value of type int is identical (`===`) to another.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function intIdentical(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isTrue($value === $integer, $message, ...$args);
    }

    /**
     * Check that a value of type int is less than or equal to another.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function intLessOrEqual(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isTrue($value <= $integer, $message, ...$args);
    }

    /**
     * Check that a value of type int is less than another.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function intLessThan(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isTrue($value < $integer, $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::intBetweenAnd`.
     *
     * @param int    $value    Current value to compare.
     * @param int    $minValue Minimum value to compare.
     * @param int    $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::intBetweenAnd()
     */
    public static function notIntBetweenAnd(int $value, int $minValue, int $maxValue, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isFalse($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::intGreaterOrEqual`.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::intGreaterOrEqual()
     */
    public static function notIntGreaterOrEqual(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isFalse($value >= $integer, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::intGreaterThan`.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::intGreaterThan()
     */
    public static function notIntGreaterThan(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isFalse($value > $integer, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::intIdentical`.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::intIdentical()
     */
    public static function notIntIdentical(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isFalse($value === $integer, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::intLessOrEqual`.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::intLessOrEqual()
     */
    public static function notIntLessOrEqual(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isFalse($value <= $integer, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::intLessThan`.
     *
     * @param int    $value   Current value to compare.
     * @param int    $integer Integer to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::intLessThan()
     */
    public static function notIntLessThan(int $value, int $integer, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $integer;

        static::isFalse($value < $integer, $message, ...$args);
    }
}