<?php

namespace jf\assert\dict;

use Locale;

/**
 * Class for handling library messages.
 */
class Messages
{
    /**
     * Messages for translating the error message of the function that threw the exception.
     *
     * @var array
     */
    public static array $messages = [];

    /**
     * Returns message with specified identifier.
     *
     * @param string $id Identifier of message to return.
     *
     * @return string|NULL
     */
    public static function get(string $id) : ?string
    {
        if (empty(static::$messages))
        {
            static::load();
        }

        return static::$messages[ $id ] ?? NULL;
    }

    /**
     * Load dictionary for translation of error messages.
     *
     * @param string $lang Language of the dictionary to be loaded.
     *
     * @return string[]
     */
    public static function load(string $lang = '') : array
    {
        $_dictDir = __DIR__;
        if (!$lang)
        {
            $_langs = array_map(fn(string $file) => basename($file, '.yaml'), glob($_dictDir . '/*.yaml'));
            $_lang  = current($_langs);
            $lang   = Locale::lookup(
                $_langs,
                Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? Locale::getDefault() ?: $_lang) ?: $_lang,
                FALSE
            ) ?: $_lang;
        }
        $_file = sprintf('%s/%s.yaml', $_dictDir, strtolower($lang));

        return is_file($_file)
            ? (static::$messages = yaml_parse_file($_file))
            : [];
    }
}