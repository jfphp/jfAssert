<?php

namespace jf\assert;

/**
 * Trait for logical comparisons between mixed types.
 */
trait TLogical
{
    /**
     * Check that a value is in the given range.
     *
     * @param mixed  $value    Current value to compare.
     * @param mixed  $minValue Minimum value to compare.
     * @param mixed  $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function betweenAnd(mixed $value, mixed $minValue, mixed $maxValue, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isTrue($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Check that a value is equal (`==`) to another.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function equal(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isTrue($value == $another, $message, ...$args);
    }

    /**
     * Check that a value is greater than or equal to another.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function greaterOrEqual(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isTrue($value >= $another, $message, ...$args);
    }

    /**
     * Check that a value is greater than another.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function greaterThan(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isTrue($value > $another, $message, ...$args);
    }

    /**
     * Check that a value is identical (`===`) to another.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function identical(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isTrue($value === $another, $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Check that a value is less than or equal to another.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function lessOrEqual(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isTrue($value <= $another, $message, ...$args);
    }

    /**
     * Check that a value is less than another.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function lessThan(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isTrue($value < $another, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::betweenAnd`.
     *
     * @param mixed  $value    Current value to compare.
     * @param mixed  $minValue Minimum value to compare.
     * @param mixed  $maxValue Maximum value to compare.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::betweenAnd()
     */
    public static function notBetweenAnd(mixed $value, mixed $minValue, mixed $maxValue, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $minValue;
        $args[] = $maxValue;

        static::isFalse($value >= $minValue && $value <= $maxValue, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::equal`.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::equal()
     */
    public static function notEqual(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isFalse($value == $another, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::greaterOrEqual`.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::greaterOrEqual()
     */
    public static function notGreaterOrEqual(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isFalse($value >= $another, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::greaterThan`.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::greaterThan()
     */
    public static function notGreaterThan(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isFalse($value > $another, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::identical`.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::identical()
     */
    public static function notIdentical(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isFalse($value === $another, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::lessOrEqual`.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::lessOrEqual()
     */
    public static function notLessOrEqual(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isFalse($value <= $another, $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::lessThan`.
     *
     * @param mixed  $value   Current value to compare.
     * @param mixed  $another Another value to compare.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::lessThan()
     */
    public static function notLessThan(mixed $value, mixed $another, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;
        $args[] = $another;

        static::isFalse($value < $another, $message, ...$args);
    }
}