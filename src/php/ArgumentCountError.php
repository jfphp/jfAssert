<?php

namespace jf\assert\php;

use ArgumentCountError as PhpArgumentCountError;
use jf\assert\TAll;

/**
 * ArgumentCountError is thrown when too few arguments are passed to a user-defined
 * function or method.
 */
class ArgumentCountError extends PhpArgumentCountError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3618586576;
}
