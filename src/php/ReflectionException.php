<?php

namespace jf\assert\php;

use jf\assert\TAll;
use ReflectionException as PhpReflectionException;

/**
 * The ReflectionException class.
 */
class ReflectionException extends PhpReflectionException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3774173198;
}
