<?php

namespace jf\assert\php;

use jf\assert\TAll;
use RangeException as PhpRangeException;

/**
 * Exception thrown to indicate range errors during program execution. Normally
 * this means there was an arithmetic error other than under/overflow. This is the
 * runtime version of DomainException.
 */
class RangeException extends PhpRangeException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1355325141;
}
