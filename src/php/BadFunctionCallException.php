<?php

namespace jf\assert\php;

use BadFunctionCallException as PhpBadFunctionCallException;
use jf\assert\TAll;

/**
 * Exception thrown if a callback refers to an undefined function or if some
 * arguments are missing.
 */
class BadFunctionCallException extends PhpBadFunctionCallException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1371564091;
}
