<?php

namespace jf\assert\php;

use jf\assert\TAll;
use ParseError as PhpParseError;

/**
 * ParseError is thrown when an error occurs while parsing PHP code, such as when
 * eval() is called.
 */
class ParseError extends PhpParseError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 544529667;
}
