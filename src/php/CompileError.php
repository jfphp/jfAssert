<?php

namespace jf\assert\php;

use CompileError as PhpCompileError;
use jf\assert\TAll;

/**
 * CompileError is thrown for some compilation errors, which formerly issued a
 * fatal error.
 */
class CompileError extends PhpCompileError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 2852907524;
}
