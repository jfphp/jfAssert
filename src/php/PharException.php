<?php

namespace jf\assert\php;

use jf\assert\TAll;
use PharException as PhpPharException;

/**
 * The PharException class provides a phar-specific exception class for try/catch blocks.
 */
class PharException extends PhpPharException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1350004536;
}
