<?php

namespace jf\assert\php;

use jf\assert\TAll;
use UnderflowException as PhpUnderflowException;

/**
 * Exception thrown when performing an invalid operation on an empty container,
 * such as removing an element.
 */
class UnderflowException extends PhpUnderflowException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 4244676799;
}
