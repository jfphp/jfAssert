<?php

namespace jf\assert\php;

use Exception as PhpException;
use jf\assert\TAll;

/**
 * Exception is the base class for all user exceptions.
 */
class Exception extends PhpException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 4248308942;
}
