<?php

namespace jf\assert\php;

use jf\assert\TAll;
use ValueError as PhpValueError;

/**
 * A ValueError is thrown when the type of an argument is correct but the value of
 * it is incorrect. For example, passing a negative integer when the function
 * expects a positive one, or passing an empty string/array when the function
 * expects it to not be empty.
 */
class ValueError extends PhpValueError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 663468903;
}
