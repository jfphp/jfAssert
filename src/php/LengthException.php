<?php

namespace jf\assert\php;

use jf\assert\TAll;
use LengthException as PhpLengthException;

/**
 * Exception thrown if a length is invalid.
 */
class LengthException extends PhpLengthException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1871060678;
}
