<?php

namespace jf\assert\php;

use jf\assert\TAll;
use SodiumException as PhpSodiumException;

/**
 * Exceptions thrown by the sodium functions.
 */
class SodiumException extends PhpSodiumException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1502755123;
}
