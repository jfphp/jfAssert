<?php

namespace jf\assert\php;

use BadMethodCallException as PhpBadMethodCallException;
use jf\assert\TAll;

/**
 * Exception thrown if a callback refers to an undefined method or if some
 * arguments are missing.
 */
class BadMethodCallException extends PhpBadMethodCallException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1333929647;
}
