<?php

namespace jf\assert\php;

use InvalidArgumentException as PhpInvalidArgumentException;
use jf\assert\TAll;

/**
 * Exception thrown if an argument is not of the expected type.
 */
class InvalidArgumentException extends PhpInvalidArgumentException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3591986259;
}
