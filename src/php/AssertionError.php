<?php

namespace jf\assert\php;

use AssertionError as PhpAssertionError;
use jf\assert\TAll;

/**
 * AssertionError is thrown when an assertion made via assert() fails.
 */
class AssertionError extends PhpAssertionError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 4245511382;
}
