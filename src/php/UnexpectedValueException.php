<?php

namespace jf\assert\php;

use jf\assert\TAll;
use UnexpectedValueException as PhpUnexpectedValueException;

/**
 * Exception thrown if a value does not match with a set of values. Typically this
 * happens when a function calls another function and expects the return value to
 * be of a certain type or value not including arithmetic or buffer related errors.
 */
class UnexpectedValueException extends PhpUnexpectedValueException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1971318510;
}
