<?php

namespace jf\assert\php;

use IntlException as PhpIntlException;
use jf\assert\TAll;

/**
 * This class is used for generating exceptions when errors occur inside intl
 * functions. Such exceptions are only generated when intl.use_exceptions is enabled.
 */
class IntlException extends PhpIntlException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1780399928;
}
