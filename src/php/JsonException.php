<?php

namespace jf\assert\php;

use jf\assert\TAll;
use JsonException as PhpJsonException;

/**
 * Exception thrown if JSON_THROW_ON_ERROR option is set for json_encode() or
 * json_decode(). code contains the error type, for possible values see
 * json_last_error().
 */
class JsonException extends PhpJsonException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3426856022;
}
