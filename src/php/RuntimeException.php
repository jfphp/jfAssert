<?php

namespace jf\assert\php;

use jf\assert\TAll;
use RuntimeException as PhpRuntimeException;

/**
 * Exception thrown if an error which can only be found on runtime occurs.
 */
class RuntimeException extends PhpRuntimeException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 2849395772;
}
