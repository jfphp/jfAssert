<?php

namespace jf\assert\php;

use jf\assert\TAll;
use OverflowException as PhpOverflowException;

/**
 * Exception thrown when adding an element to a full container.
 */
class OverflowException extends PhpOverflowException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3953275284;
}
