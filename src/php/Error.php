<?php

namespace jf\assert\php;

use Error as PhpError;
use jf\assert\TAll;

/**
 * Error is the base class for all internal PHP errors.
 */
class Error extends PhpError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 2619118453;
}
