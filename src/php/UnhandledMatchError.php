<?php

namespace jf\assert\php;

use jf\assert\TAll;
use UnhandledMatchError as PhpUnhandledMatchError;

/**
 * An UnhandledMatchError is thrown when the subject passed to a match expression
 * is not handled by any arm of the match expression.
 */
class UnhandledMatchError extends PhpUnhandledMatchError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3036329137;
}
