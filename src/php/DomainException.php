<?php

namespace jf\assert\php;

use DomainException as PhpDomainException;
use jf\assert\TAll;

/**
 * Exception thrown if a value does not adhere to a defined valid data domain.
 */
class DomainException extends PhpDomainException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1219704959;
}
