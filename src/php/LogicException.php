<?php

namespace jf\assert\php;

use jf\assert\TAll;
use LogicException as PhpLogicException;

/**
 * Exception that represents error in the program logic. This kind of exception
 * should lead directly to a fix in your code.
 */
class LogicException extends PhpLogicException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3318127193;
}
