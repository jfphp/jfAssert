<?php

namespace jf\assert\php;

use ErrorException as PhpErrorException;
use jf\assert\TAll;

/**
 * An Error Exception.
 */
class ErrorException extends PhpErrorException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 752425412;
}
