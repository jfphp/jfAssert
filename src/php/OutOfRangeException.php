<?php

namespace jf\assert\php;

use jf\assert\TAll;
use OutOfRangeException as PhpOutOfRangeException;

/**
 * Exception thrown when an illegal index was requested. This represents errors
 * that should be detected at compile time.
 */
class OutOfRangeException extends PhpOutOfRangeException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 2945599577;
}
