<?php

namespace jf\assert\php;

use DivisionByZeroError as PhpDivisionByZeroError;
use jf\assert\TAll;

/**
 * DivisionByZeroError is thrown when an attempt is made to divide a number by zero.
 */
class DivisionByZeroError extends PhpDivisionByZeroError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 3090415957;
}
