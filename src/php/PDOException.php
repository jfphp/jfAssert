<?php

namespace jf\assert\php;

use jf\assert\TAll;
use PDOException as PhpPDOException;

/**
 * Represents an error raised by PDO. You should not throw a PDOException from your
 * own code. See Exceptions for more information about Exceptions in PHP.
 */
class PDOException extends PhpPDOException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 1438626371;
}
