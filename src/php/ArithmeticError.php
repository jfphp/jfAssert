<?php

namespace jf\assert\php;

use ArithmeticError as PhpArithmeticError;
use jf\assert\TAll;

/**
 * ArithmeticError is thrown when an error occurs while performing mathematical
 * operations. These errors include attempting to perform a bitshift by a negative
 * amount, and any call to intdiv() that would result in a value outside the
 * possible bounds of an int.
 */
class ArithmeticError extends PhpArithmeticError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 996674865;
}
