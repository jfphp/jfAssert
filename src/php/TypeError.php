<?php

namespace jf\assert\php;

use jf\assert\TAll;
use TypeError as PhpTypeError;

/**
 * A TypeError may be thrown when.
 */
class TypeError extends PhpTypeError
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 2496111813;
}
