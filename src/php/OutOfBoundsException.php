<?php

namespace jf\assert\php;

use jf\assert\TAll;
use OutOfBoundsException as PhpOutOfBoundsException;

/**
 * Exception thrown if a value is not a valid key. This represents errors that
 * cannot be detected at compile time.
 */
class OutOfBoundsException extends PhpOutOfBoundsException
{
    use TAll;

    /**
     * @inheritdoc
     */
    public const CODE = 200070345;
}
