<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `classobj`.
 *
 * @see https://www.php.net/manual/en/book.classobj.php
 */
trait TClassObj
{
    /**
     * Creates an alias for a class.
     *
     * @param string $class    The original class.
     * @param string $alias    The alias name for the class.
     * @param bool   $autoload Whether to autoload if the original class is not found.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function classAlias(string $class, string $alias, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $class;
        $args[] = $alias;
        $args[] = $autoload;

        static::isTrue(class_alias($class, $alias, $autoload), $message, ...$args);
    }

    /**
     * Checks if the class has been defined.
     *
     * @param string $class    The class name. The name is matched in a case-insensitive manner.
     * @param bool   $autoload Whether to call `__autoload` by default.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function classExists(string $class, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $class;
        $args[] = $autoload;

        static::isTrue(class_exists($class, $autoload), $message, ...$args);
    }

    /**
     * Checks if the enum has been defined.
     *
     * @param string $enum     The enum name. The name is matched in a case-insensitive manner.
     * @param bool   $autoload Whether to call `__autoload` by default.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function enumExists(string $enum, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $enum;
        $args[] = $autoload;

        static::isTrue(enum_exists($enum, $autoload), $message, ...$args);
    }

    /**
     * Checks if the interface has been defined.
     *
     * @param string $interface The interface name.
     * @param bool   $autoload  Whether to call `__autoload` or not by default.
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function interfaceExists(string $interface, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $interface;
        $args[] = $autoload;

        static::isTrue(interface_exists($interface, $autoload), $message, ...$args);
    }

    /**
     * Checks if the object is of this class or has this class as one of its parents.
     *
     * @param mixed  $objectOrClass A class name or an object instance.
     * @param string $class         The class name.
     * @param bool   $allowString   If this parameter set to `false`, string class name as `object_or_class` is not allowed.
     *                              This also prevents from calling autoloader if the class doesn't exist.
     * @param string $message       Message of the exception.
     * @param mixed  ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isA(mixed $objectOrClass, string $class, bool $allowString = FALSE, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $class;
        $args[] = $allowString;

        static::isTrue(is_a($objectOrClass, $class, $allowString), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Checks if the object has this class as one of its parents or implements it.
     *
     * @param mixed  $objectOrClass A class name or an object instance. No error is generated if the class does not exist.
     * @param string $class         The class name.
     * @param bool   $allowString   If this parameter set to false, string class name as `object_or_class` is not allowed.
     *                              This also prevents from calling autoloader if the class doesn't exist.
     * @param string $message       Message of the exception.
     * @param mixed  ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isSubclassOf(mixed $objectOrClass, string $class, bool $allowString = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $class;
        $args[] = $allowString;

        static::isTrue(is_subclass_of($objectOrClass, $class, $allowString), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Checks if the class method exists.
     *
     * @param object|string $objectOrClass An object instance or a class name.
     * @param string        $method        The method name.
     * @param string        $message       Message of the exception.
     * @param mixed         ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function methodExists(object|string $objectOrClass, string $method, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $method;

        static::isTrue(method_exists($objectOrClass, $method), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::classAlias`.
     *
     * @param string $class    The original class.
     * @param string $alias    The alias name for the class.
     * @param bool   $autoload Whether to autoload if the original class is not found.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::classAlias()
     */
    public static function notClassAlias(string $class, string $alias, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $class;
        $args[] = $alias;
        $args[] = $autoload;

        static::isFalse(class_alias($class, $alias, $autoload), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::classExists`.
     *
     * @param string $class    The class name. The name is matched in a case-insensitive manner.
     * @param bool   $autoload Whether to call `__autoload` by default.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::classExists()
     */
    public static function notClassExists(string $class, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $class;
        $args[] = $autoload;

        static::isFalse(class_exists($class, $autoload), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::enumExists`.
     *
     * @param string $enum     The enum name. The name is matched in a case-insensitive manner.
     * @param bool   $autoload Whether to call `__autoload` by default.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::enumExists()
     */
    public static function notEnumExists(string $enum, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $enum;
        $args[] = $autoload;

        static::isFalse(enum_exists($enum, $autoload), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::interfaceExists`.
     *
     * @param string $interface The interface name.
     * @param bool   $autoload  Whether to call `__autoload` or not by default.
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::interfaceExists()
     */
    public static function notInterfaceExists(string $interface, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $interface;
        $args[] = $autoload;

        static::isFalse(interface_exists($interface, $autoload), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isA`.
     *
     * @param mixed  $objectOrClass A class name or an object instance.
     * @param string $class         The class name.
     * @param bool   $allowString   If this parameter set to `false`, string class name as `object_or_class` is not allowed.
     *                              This also prevents from calling autoloader if the class doesn't exist.
     * @param string $message       Message of the exception.
     * @param mixed  ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isA()
     */
    public static function notIsA(mixed $objectOrClass, string $class, bool $allowString = FALSE, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $class;
        $args[] = $allowString;

        static::isFalse(is_a($objectOrClass, $class, $allowString), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isSubclassOf`.
     *
     * @param mixed  $objectOrClass A class name or an object instance. No error is generated if the class does not exist.
     * @param string $class         The class name.
     * @param bool   $allowString   If this parameter set to false, string class name as `object_or_class` is not allowed.
     *                              This also prevents from calling autoloader if the class doesn't exist.
     * @param string $message       Message of the exception.
     * @param mixed  ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isSubclassOf()
     */
    public static function notIsSubclassOf(mixed $objectOrClass, string $class, bool $allowString = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $class;
        $args[] = $allowString;

        static::isFalse(is_subclass_of($objectOrClass, $class, $allowString), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::methodExists`.
     *
     * @param object|string $objectOrClass An object instance or a class name.
     * @param string        $method        The method name.
     * @param string        $message       Message of the exception.
     * @param mixed         ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::methodExists()
     */
    public static function notMethodExists(object|string $objectOrClass, string $method, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $method;

        static::isFalse(method_exists($objectOrClass, $method), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::propertyExists`.
     *
     * @param object|string $objectOrClass The class name or an object of the class to test for.
     * @param string        $property      The name of the property.
     * @param string        $message       Message of the exception.
     * @param mixed         ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::propertyExists()
     */
    public static function notPropertyExists(object|string $objectOrClass, string $property, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $property;

        static::isFalse(property_exists($objectOrClass, $property), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::traitExists`.
     *
     * @param string $trait    Name of the trait to check.
     * @param bool   $autoload Whether to autoload if not already loaded.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::traitExists()
     */
    public static function notTraitExists(string $trait, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $trait;
        $args[] = $autoload;

        static::isFalse(trait_exists($trait, $autoload), $message, ...$args);
    }

    /**
     * Checks if the object or class has a property.
     *
     * @param object|string $objectOrClass The class name or an object of the class to test for.
     * @param string        $property      The name of the property.
     * @param string        $message       Message of the exception.
     * @param mixed         ...$args       Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function propertyExists(object|string $objectOrClass, string $property, string $message = '', mixed ...$args) : void
    {
        $args[] = $objectOrClass;
        $args[] = $property;

        static::isTrue(property_exists($objectOrClass, $property), $message, ...$args);
    }

    /**
     * Checks if the trait exists.
     *
     * @param string $trait    Name of the trait to check.
     * @param bool   $autoload Whether to autoload if not already loaded.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function traitExists(string $trait, bool $autoload = TRUE, string $message = '', mixed ...$args) : void
    {
        $args[] = $trait;
        $args[] = $autoload;

        static::isTrue(trait_exists($trait, $autoload), $message, ...$args);
    }
}