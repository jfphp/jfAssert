<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `ctype`.
 *
 * @see https://www.php.net/manual/en/book.ctype.php
 */
trait TCtype
{
    /**
     * Check for alphanumeric character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isAlnum(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_alnum($text), $message, ...$args);
    }

    /**
     * Check for alphabetic character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isAlpha(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_alpha($text), $message, ...$args);
    }

    /**
     * Check for control character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isCntrl(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_cntrl($text), $message, ...$args);
    }

    /**
     * Check for numeric character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isDigit(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_digit($text), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Check for any printable character(s) except space.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isGraph(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_graph($text), $message, ...$args);
    }

    /**
     * Check for lowercase character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isLower(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_lower($text), $message, ...$args);
    }

    /**
     * Check for printable character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isPrint(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_print($text), $message, ...$args);
    }

    /**
     * Check for any printable character which is not whitespace or an alphanumeric character.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isPunct(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_punct($text), $message, ...$args);
    }

    /**
     * Check for whitespace character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isSpace(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_space($text), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Check for uppercase character(s).
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isUpper(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_upper($text), $message, ...$args);
    }

    /**
     * Check for character(s) representing a hexadecimal digit.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isXdigit(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isTrue(ctype_xdigit($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isAlnum`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isAlnum()
     */
    public static function notIsAlnum(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_alnum($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isAlpha`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isAlpha()
     */
    public static function notIsAlpha(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_alpha($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isCntrl`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isCntrl()
     */
    public static function notIsCntrl(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_cntrl($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isDigit`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isDigit()
     */
    public static function notIsDigit(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_digit($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isGraph`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isGraph()
     */
    public static function notIsGraph(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_graph($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isLower`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isLower()
     */
    public static function notIsLower(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_lower($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isPrint`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isPrint()
     */
    public static function notIsPrint(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_print($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isPunct`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isPunct()
     */
    public static function notIsPunct(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_punct($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isSpace`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isSpace()
     */
    public static function notIsSpace(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_space($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isUpper`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isUpper()
     */
    public static function notIsUpper(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_upper($text), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isXdigit`.
     *
     * @param mixed  $text    The tested string. Note: If an int between -128 and 255 inclusive is provided, it is
     *                        interpreted as the ASCII value of a single character (negative values have 256 added in
     *                        order to allow characters in the Extended ASCII range). Any other integer is interpreted
     *                        as a string containing the decimal digits of the integer. Warning As of PHP 8.1.0, passing
     *                        a non-string argument is deprecated. In the future, the argument will be interpreted as a
     *                        string instead of an ASCII codepoint. Depending on the intended behavior, the argument
     *                        should either be cast to string or an explicit call to `chr()` should be made.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isXdigit()
     */
    public static function notIsXdigit(mixed $text, string $message = '', mixed ...$args) : void
    {
        $args[] = $text;

        static::isFalse(ctype_xdigit($text), $message, ...$args);
    }
}