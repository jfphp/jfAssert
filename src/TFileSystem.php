<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `filesystem`.
 *
 * @see https://www.php.net/manual/en/book.filesystem.php
 */
trait TFileSystem
{
    /**
     * Changes file group.
     *
     * @param string     $filename Path to the file.
     * @param int|string $group    A group name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function chgrp(string $filename, int|string $group, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $group;

        static::isTrue(chgrp($filename, $group), $message, ...$args);
    }

    /**
     * Changes file mode.
     *
     * @param string $filename    Path to the file.
     * @param int    $permissions Note that `permissions` is not automatically assumed to be an octal value, so to ensure
     *                            the expected operation, you need to prefix `permissions` with a zero (0). Strings such as
     *                            "g+w" will not work properly. The `permissions` parameter consists of three octal number
     *                            components specifying access restrictions for the owner, the user group in which the owner
     *                            is in, and to everybody else in this order. One component can be computed by adding up the
     *                            needed permissions for that target user base. Number 1 means that you grant execute
     *                            rights, number 2 means that you make the file writeable, number 4 means that you make the
     *                            file readable. Add up these numbers to specify needed rights. You can also read more about
     *                            modes on Unix systems with 'man 1 chmod' and 'man 2 chmod'.
     * @param string $message     Message of the exception.
     * @param mixed  ...$args     Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function chmod(string $filename, int $permissions, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $permissions;

        static::isTrue(chmod($filename, $permissions), $message, ...$args);
    }

    /**
     * Changes file owner.
     *
     * @param string     $filename Path to the file.
     * @param int|string $user     A user name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function chown(string $filename, int|string $user, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $user;

        static::isTrue(chown($filename, $user), $message, ...$args);
    }

    /**
     * Copies file.
     *
     * @param string        $from    Path to the source file.
     * @param string        $to      The destination path. If `to` is a URL, the copy operation may fail if the wrapper does
     *                               not support overwriting of existing files. Warning If the destination file already exists,
     *                               it will be overwritten.
     * @param resource|NULL $context A valid context resource created with `stream_context_create()`.
     * @param string        $message Message of the exception.
     * @param mixed         ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function copy(string $from, string $to, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $from;
        $args[] = $to;
        $args[] = $context;

        static::isTrue(copy($from, $to, $context), $message, ...$args);
    }

    /**
     * Closes an open file pointer.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()`.
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function fclose($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isTrue(fclose($stream), $message, ...$args);
    }

    /**
     * Synchronizes data (but not meta-data) to the file.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function fdatasync($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isTrue(fdatasync($stream), $message, ...$args);
    }

    /**
     * Tests for end-of-file on a file pointer.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function feof($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isTrue(feof($stream), $message, ...$args);
    }

    /**
     * Flushes the output to a file.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function fflush($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isTrue(fflush($stream), $message, ...$args);
    }

    /**
     * Checks whether a file or directory exists.
     *
     * @param string $filename Path to the file or directory. On windows, use //computername/share/filename or
     *                         \\computername\share\filename to check files on network shares.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function fileExists(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(file_exists($filename), $message, ...$args);
    }

    /**
     * Portable advisory file locking.
     *
     * @param resource $stream     A file system pointer resource that is typically created using `fopen()`.
     * @param int      $operation  `operation` is one of the following: `LOCK_SH` to acquire a shared lock (reader).
     *                             `LOCK_EX` to acquire an exclusive lock (writer). `LOCK_UN` to release a lock (shared or
     *                             exclusive). It is also possible to add `LOCK_NB` as a bitmask to one of the above
     *                             operations, if flock() should not block during the locking attempt.
     * @param int|NULL $wouldBlock The optional third argument is set to 1 if the lock would block (EWOULDBLOCK errno
     *                             condition).
     * @param string   $message    Message of the exception.
     * @param mixed    ...$args    Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function flock($stream, int $operation, ?int &$wouldBlock = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;
        $args[] = $operation;
        $args[] = $wouldBlock;

        static::isTrue(flock($stream, $operation, $wouldBlock), $message, ...$args);
    }

    /**
     * Match filename against a pattern.
     *
     * @param string $pattern  The shell wildcard pattern.
     * @param string $filename The tested string. This function is especially useful for filenames, but may also be used
     *                         on regular strings. The average user may be used to shell patterns or at least in their
     *                         simplest form to `'?'` and `'*'` wildcards so using fnmatch() instead of `preg_match()`
     *                         for frontend search expression input may be way more convenient for non-programming users.
     * @param int    $flags    The value of `flags` can be any combination of the following flags, joined with the
     *                         `binary OR (|) operator`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function fnmatch(string $pattern, string $filename, int $flags = 0, string $message = '', mixed ...$args) : void
    {
        $args[] = $pattern;
        $args[] = $filename;
        $args[] = $flags;

        static::isTrue(fnmatch($pattern, $filename, $flags), $message, ...$args);
    }

    /**
     * Synchronizes changes to the file (including meta-data).
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function fsync($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isTrue(fsync($stream), $message, ...$args);
    }

    /**
     * Truncates a file to a given length.
     *
     * @param resource $stream  The file pointer. Note: The `stream` must be open for writing.
     * @param int      $size    The size to truncate to. Note: If `size` is larger than the file then the file is extended
     *                          with null bytes. If `size` is smaller than the file then the file is truncated to that
     *                          size.
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function ftruncate($stream, int $size, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;
        $args[] = $size;

        static::isTrue(ftruncate($stream, $size), $message, ...$args);
    }

    /**
     * Tells whether the filename is a directory.
     *
     * @param string $filename Path to the file. If `filename` is a relative filename, it will be checked relative to the
     *                         current working directory. If `filename` is a symbolic or hard link then the link will be
     *                         resolved and checked. If you have enabled `open_basedir` further restrictions may apply.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isDir(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_dir($filename), $message, ...$args);
    }

    /**
     * Tells whether the filename is executable.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isExecutable(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_executable($filename), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Tells whether the filename is a regular file.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isFile(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_file($filename), $message, ...$args);
    }

    /**
     * Tells whether the filename is a symbolic link.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isLink(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_link($filename), $message, ...$args);
    }

    /**
     * Tells whether a file exists and is readable.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isReadable(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_readable($filename), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Tells whether the file was uploaded via HTTP POST.
     *
     * @param string $filename The filename being checked.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isUploadedFile(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_uploaded_file($filename), $message, ...$args);
    }

    /**
     * Tells whether the filename is writable.
     *
     * @param string $filename The filename being checked.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isWritable(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isTrue(is_writable($filename), $message, ...$args);
    }

    /**
     * Changes group ownership of symlink.
     *
     * @param string     $filename Path to the symlink.
     * @param int|string $group    The group specified by name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function lchgrp(string $filename, int|string $group, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $group;

        static::isTrue(lchgrp($filename, $group), $message, ...$args);
    }

    /**
     * Changes user ownership of symlink.
     *
     * @param string     $filename Path to the file.
     * @param int|string $user     User name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function lchown(string $filename, int|string $user, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $user;

        static::isTrue(lchown($filename, $user), $message, ...$args);
    }

    /**
     * Create a hard link.
     *
     * @param string $target  Target of the link.
     * @param string $link    The link name.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function link(string $target, string $link, string $message = '', mixed ...$args) : void
    {
        $args[] = $target;
        $args[] = $link;

        static::isTrue(link($target, $link), $message, ...$args);
    }

    /**
     * Makes directory.
     *
     * @param string        $directory   The directory path.
     * @param int           $permissions The permissions are 0777 by default, which means the widest possible access. For more
     *                                   information on permissions, read the details on the `chmod()` page. Note: `permissions` is
     *                                   ignored on Windows. Note that you probably want to specify the `permissions` as an octal
     *                                   number, which means it should have a leading zero. The `permissions` is also modified by
     *                                   the current umask, which you can change using `umask()`.
     * @param bool          $recursive   Allows the creation of nested directories specified in the `directory`.
     * @param resource|NULL $context     A `context stream` resource.
     * @param string        $message     Message of the exception.
     * @param mixed         ...$args     Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function mkdir(string $directory, int $permissions = 0777, bool $recursive = FALSE, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $directory;
        $args[] = $permissions;
        $args[] = $recursive;
        $args[] = $context;

        static::isTrue(mkdir($directory, $permissions, $recursive, $context), $message, ...$args);
    }

    /**
     * Moves an uploaded file to a new location.
     *
     * @param string $from    The filename of the uploaded file.
     * @param string $to      The destination of the moved file.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function moveUploadedFile(string $from, string $to, string $message = '', mixed ...$args) : void
    {
        $args[] = $from;
        $args[] = $to;

        static::isTrue(move_uploaded_file($from, $to), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::chgrp`.
     *
     * @param string     $filename Path to the file.
     * @param int|string $group    A group name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::chgrp()
     */
    public static function notChgrp(string $filename, int|string $group, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $group;

        static::isFalse(chgrp($filename, $group), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::chmod`.
     *
     * @param string $filename    Path to the file.
     * @param int    $permissions Note that `permissions` is not automatically assumed to be an octal value, so to ensure
     *                            the expected operation, you need to prefix `permissions` with a zero (0). Strings such as
     *                            "g+w" will not work properly. The `permissions` parameter consists of three octal number
     *                            components specifying access restrictions for the owner, the user group in which the owner
     *                            is in, and to everybody else in this order. One component can be computed by adding up the
     *                            needed permissions for that target user base. Number 1 means that you grant execute
     *                            rights, number 2 means that you make the file writeable, number 4 means that you make the
     *                            file readable. Add up these numbers to specify needed rights. You can also read more about
     *                            modes on Unix systems with 'man 1 chmod' and 'man 2 chmod'.
     * @param string $message     Message of the exception.
     * @param mixed  ...$args     Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::chmod()
     */
    public static function notChmod(string $filename, int $permissions, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $permissions;

        static::isFalse(chmod($filename, $permissions), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::chown`.
     *
     * @param string     $filename Path to the file.
     * @param int|string $user     A user name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::chown()
     */
    public static function notChown(string $filename, int|string $user, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $user;

        static::isFalse(chown($filename, $user), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::copy`.
     *
     * @param string        $from    Path to the source file.
     * @param string        $to      The destination path. If `to` is a URL, the copy operation may fail if the wrapper does
     *                               not support overwriting of existing files. Warning If the destination file already exists,
     *                               it will be overwritten.
     * @param resource|NULL $context A valid context resource created with `stream_context_create()`.
     * @param string        $message Message of the exception.
     * @param mixed         ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::copy()
     */
    public static function notCopy(string $from, string $to, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $from;
        $args[] = $to;
        $args[] = $context;

        static::isFalse(copy($from, $to, $context), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::fclose`.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()`.
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::fclose()
     */
    public static function notFclose($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isFalse(fclose($stream), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::fdatasync`.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::fdatasync()
     */
    public static function notFdatasync($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isFalse(fdatasync($stream), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::feof`.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::feof()
     */
    public static function notFeof($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isFalse(feof($stream), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::fflush`.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::fflush()
     */
    public static function notFflush($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isFalse(fflush($stream), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::fileExists`.
     *
     * @param string $filename Path to the file or directory. On windows, use //computername/share/filename or
     *                         \\computername\share\filename to check files on network shares.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::fileExists()
     */
    public static function notFileExists(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(file_exists($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::flock`.
     *
     * @param resource $stream     A file system pointer resource that is typically created using `fopen()`.
     * @param int      $operation  `operation` is one of the following: `LOCK_SH` to acquire a shared lock (reader).
     *                             `LOCK_EX` to acquire an exclusive lock (writer). `LOCK_UN` to release a lock (shared or
     *                             exclusive). It is also possible to add `LOCK_NB` as a bitmask to one of the above
     *                             operations, if flock() should not block during the locking attempt.
     * @param int|NULL $wouldBlock The optional third argument is set to 1 if the lock would block (EWOULDBLOCK errno
     *                             condition).
     * @param string   $message    Message of the exception.
     * @param mixed    ...$args    Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::flock()
     */
    public static function notFlock($stream, int $operation, ?int &$wouldBlock = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;
        $args[] = $operation;
        $args[] = $wouldBlock;

        static::isFalse(flock($stream, $operation, $wouldBlock), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::fnmatch`.
     *
     * @param string $pattern  The shell wildcard pattern.
     * @param string $filename The tested string. This function is especially useful for filenames, but may also be used
     *                         on regular strings. The average user may be used to shell patterns or at least in their
     *                         simplest form to `'?'` and `'*'` wildcards so using fnmatch() instead of `preg_match()`
     *                         for frontend search expression input may be way more convenient for non-programming users.
     * @param int    $flags    The value of `flags` can be any combination of the following flags, joined with the
     *                         `binary OR (|) operator`.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::fnmatch()
     */
    public static function notFnmatch(string $pattern, string $filename, int $flags = 0, string $message = '', mixed ...$args) : void
    {
        $args[] = $pattern;
        $args[] = $filename;
        $args[] = $flags;

        static::isFalse(fnmatch($pattern, $filename, $flags), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::fsync`.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`
     *                          or `fsockopen()` (and not yet closed by `fclose()`).
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::fsync()
     */
    public static function notFsync($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isFalse(fsync($stream), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::ftruncate`.
     *
     * @param resource $stream  The file pointer. Note: The `stream` must be open for writing.
     * @param int      $size    The size to truncate to. Note: If `size` is larger than the file then the file is extended
     *                          with null bytes. If `size` is smaller than the file then the file is truncated to that
     *                          size.
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::ftruncate()
     */
    public static function notFtruncate($stream, int $size, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;
        $args[] = $size;

        static::isFalse(ftruncate($stream, $size), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isDir`.
     *
     * @param string $filename Path to the file. If `filename` is a relative filename, it will be checked relative to the
     *                         current working directory. If `filename` is a symbolic or hard link then the link will be
     *                         resolved and checked. If you have enabled `open_basedir` further restrictions may apply.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isDir()
     */
    public static function notIsDir(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_dir($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isExecutable`.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isExecutable()
     */
    public static function notIsExecutable(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_executable($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isFile`.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isFile()
     */
    public static function notIsFile(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_file($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isLink`.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isLink()
     */
    public static function notIsLink(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_link($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isReadable`.
     *
     * @param string $filename Path to the file.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isReadable()
     */
    public static function notIsReadable(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_readable($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isUploadedFile`.
     *
     * @param string $filename The filename being checked.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isUploadedFile()
     */
    public static function notIsUploadedFile(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_uploaded_file($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::isWritable`.
     *
     * @param string $filename The filename being checked.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::isWritable()
     */
    public static function notIsWritable(string $filename, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;

        static::isFalse(is_writable($filename), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::lchgrp`.
     *
     * @param string     $filename Path to the symlink.
     * @param int|string $group    The group specified by name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::lchgrp()
     */
    public static function notLchgrp(string $filename, int|string $group, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $group;

        static::isFalse(lchgrp($filename, $group), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::lchown`.
     *
     * @param string     $filename Path to the file.
     * @param int|string $user     User name or number.
     * @param string     $message  Message of the exception.
     * @param mixed      ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::lchown()
     */
    public static function notLchown(string $filename, int|string $user, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $user;

        static::isFalse(lchown($filename, $user), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::link`.
     *
     * @param string $target  Target of the link.
     * @param string $link    The link name.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::link()
     */
    public static function notLink(string $target, string $link, string $message = '', mixed ...$args) : void
    {
        $args[] = $target;
        $args[] = $link;

        static::isFalse(link($target, $link), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::mkdir`.
     *
     * @param string        $directory   The directory path.
     * @param int           $permissions The permissions are 0777 by default, which means the widest possible access. For more
     *                                   information on permissions, read the details on the `chmod()` page. Note: `permissions` is
     *                                   ignored on Windows. Note that you probably want to specify the `permissions` as an octal
     *                                   number, which means it should have a leading zero. The `permissions` is also modified by
     *                                   the current umask, which you can change using `umask()`.
     * @param bool          $recursive   Allows the creation of nested directories specified in the `directory`.
     * @param resource|NULL $context     A `context stream` resource.
     * @param string        $message     Message of the exception.
     * @param mixed         ...$args     Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::mkdir()
     */
    public static function notMkdir(string $directory, int $permissions = 0777, bool $recursive = FALSE, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $directory;
        $args[] = $permissions;
        $args[] = $recursive;
        $args[] = $context;

        static::isFalse(mkdir($directory, $permissions, $recursive, $context), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::moveUploadedFile`.
     *
     * @param string $from    The filename of the uploaded file.
     * @param string $to      The destination of the moved file.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::moveUploadedFile()
     */
    public static function notMoveUploadedFile(string $from, string $to, string $message = '', mixed ...$args) : void
    {
        $args[] = $from;
        $args[] = $to;

        static::isFalse(move_uploaded_file($from, $to), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::rename`.
     *
     * @param string        $from    The old name. Note: The wrapper used in `from` must match the wrapper used in `to`.
     * @param string        $to      The new name. Note: On Windows, if `to` already exists, it must be writable. Otherwise
     *                               rename() fails and issues `E_WARNING`.
     * @param resource|NULL $context A `context stream` resource.
     * @param string        $message Message of the exception.
     * @param mixed         ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::rename()
     */
    public static function notRename(string $from, string $to, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $from;
        $args[] = $to;
        $args[] = $context;

        static::isFalse(rename($from, $to, $context), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::rewind`.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`.
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::rewind()
     */
    public static function notRewind($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isFalse(rewind($stream), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::rmdir`.
     *
     * @param string        $directory Path to the directory.
     * @param resource|NULL $context   A `context stream` resource.
     * @param string        $message   Message of the exception.
     * @param mixed         ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::rmdir()
     */
    public static function notRmdir(string $directory, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $directory;
        $args[] = $context;

        static::isFalse(rmdir($directory, $context), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::symlink`.
     *
     * @param string $target  Target of the link.
     * @param string $link    The link name.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::symlink()
     */
    public static function notSymlink(string $target, string $link, string $message = '', mixed ...$args) : void
    {
        $args[] = $target;
        $args[] = $link;

        static::isFalse(symlink($target, $link), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::touch`.
     *
     * @param string   $filename The name of the file being touched.
     * @param int|NULL $mtime    The touch time. If `mtime` is `null`, the current system `time()` is used.
     * @param int|NULL $atime    If not `null`, the access time of the given filename is set to the value of `atime`.
     *                           Otherwise, it is set to the value passed to the `mtime` parameter. If both are `null`, the
     *                           current system time is used.
     * @param string   $message  Message of the exception.
     * @param mixed    ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::touch()
     */
    public static function notTouch(string $filename, ?int $mtime = NULL, ?int $atime = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $mtime;
        $args[] = $atime;

        static::isFalse(touch($filename, $mtime, $atime), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::unlink`.
     *
     * @param string        $filename Path to the file. If the file is a symlink, the symlink will be deleted. On Windows, to
     *                                delete a symlink to a directory, `rmdir()` has to be used instead.
     * @param resource|NULL $context  A `context stream` resource.
     * @param string        $message  Message of the exception.
     * @param mixed         ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::unlink()
     */
    public static function notUnlink(string $filename, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $context;

        static::isFalse(unlink($filename, $context), $message, ...$args);
    }

    /**
     * Renames a file or directory.
     *
     * @param string        $from    The old name. Note: The wrapper used in `from` must match the wrapper used in `to`.
     * @param string        $to      The new name. Note: On Windows, if `to` already exists, it must be writable. Otherwise
     *                               rename() fails and issues `E_WARNING`.
     * @param resource|NULL $context A `context stream` resource.
     * @param string        $message Message of the exception.
     * @param mixed         ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function rename(string $from, string $to, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $from;
        $args[] = $to;
        $args[] = $context;

        static::isTrue(rename($from, $to, $context), $message, ...$args);
    }

    /**
     * Rewind the position of a file pointer.
     *
     * @param resource $stream  The file pointer must be valid, and must point to a file successfully opened by `fopen()`.
     * @param string   $message Message of the exception.
     * @param mixed    ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function rewind($stream, string $message = '', mixed ...$args) : void
    {
        $args[] = $stream;

        static::isTrue(rewind($stream), $message, ...$args);
    }

    /**
     * Removes directory.
     *
     * @param string        $directory Path to the directory.
     * @param resource|NULL $context   A `context stream` resource.
     * @param string        $message   Message of the exception.
     * @param mixed         ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function rmdir(string $directory, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $directory;
        $args[] = $context;

        static::isTrue(rmdir($directory, $context), $message, ...$args);
    }

    /**
     * Creates a symbolic link.
     *
     * @param string $target  Target of the link.
     * @param string $link    The link name.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function symlink(string $target, string $link, string $message = '', mixed ...$args) : void
    {
        $args[] = $target;
        $args[] = $link;

        static::isTrue(symlink($target, $link), $message, ...$args);
    }

    /**
     * Sets access and modification time of file.
     *
     * @param string   $filename The name of the file being touched.
     * @param int|NULL $mtime    The touch time. If `mtime` is `null`, the current system `time()` is used.
     * @param int|NULL $atime    If not `null`, the access time of the given filename is set to the value of `atime`.
     *                           Otherwise, it is set to the value passed to the `mtime` parameter. If both are `null`, the
     *                           current system time is used.
     * @param string   $message  Message of the exception.
     * @param mixed    ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function touch(string $filename, ?int $mtime = NULL, ?int $atime = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $mtime;
        $args[] = $atime;

        static::isTrue(touch($filename, $mtime, $atime), $message, ...$args);
    }

    /**
     * Deletes a file.
     *
     * @param string        $filename Path to the file. If the file is a symlink, the symlink will be deleted. On Windows, to
     *                                delete a symlink to a directory, `rmdir()` has to be used instead.
     * @param resource|NULL $context  A `context stream` resource.
     * @param string        $message  Message of the exception.
     * @param mixed         ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function unlink(string $filename, $context = NULL, string $message = '', mixed ...$args) : void
    {
        $args[] = $filename;
        $args[] = $context;

        static::isTrue(unlink($filename, $context), $message, ...$args);
    }
}