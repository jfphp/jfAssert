<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `info`.
 *
 * @see https://www.php.net/manual/en/book.info.php
 */
trait TInfo
{
    /**
     * Find out whether an extension is loaded.
     *
     * @param string $extension The extension name. This parameter is case-insensitive. You can see the names of various
     *                          extensions by using `phpinfo()` or if you're using the `CGI` or `CLI` version of PHP you
     *                          can use the -m switch to list all available extensions: $ php -m [PHP Modules] xml
     *                          tokenizer standard sockets session posix pcre overload mysql mbstring ctype [Zend
     *                          Modules].
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function extensionLoaded(string $extension, string $message = '', mixed ...$args) : void
    {
        $args[] = $extension;

        static::isTrue(extension_loaded($extension), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::extensionLoaded`.
     *
     * @param string $extension The extension name. This parameter is case-insensitive. You can see the names of various
     *                          extensions by using `phpinfo()` or if you're using the `CGI` or `CLI` version of PHP you
     *                          can use the -m switch to list all available extensions: $ php -m [PHP Modules] xml
     *                          tokenizer standard sockets session posix pcre overload mysql mbstring ctype [Zend
     *                          Modules].
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::extensionLoaded()
     */
    public static function notExtensionLoaded(string $extension, string $message = '', mixed ...$args) : void
    {
        $args[] = $extension;

        static::isFalse(extension_loaded($extension), $message, ...$args);
    }

    /**
     * Evaluates the inverse condition of the method `static::versionCompare`.
     *
     * @param string $version1 First version number.
     * @param string $version2 Second version number.
     * @param string $operator An optional operator. The possible operators are: `&lt;`, `lt`, `&lt;=`, `le`, `&gt;`,
     *                         `gt`, `&gt;=`, `ge`, `==`, `=`, `eq`, `!=`, `&lt;&gt;`, `ne` respectively. This parameter
     *                         is case-sensitive, values should be lowercase.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::versionCompare()
     */
    public static function notVersionCompare(string $version1, string $version2, string $operator = 'eq', string $message = '', mixed ...$args) : void
    {
        $args[] = $version1;
        $args[] = $version2;
        $args[] = $operator;

        static::isFalse(version_compare($version1, $version2, $operator), $message, ...$args);
    }

    /**
     * Compares two "PHP-standardized" version number strings.
     *
     * @param string $version1 First version number.
     * @param string $version2 Second version number.
     * @param string $operator An optional operator. The possible operators are: `&lt;`, `lt`, `&lt;=`, `le`, `&gt;`,
     *                         `gt`, `&gt;=`, `ge`, `==`, `=`, `eq`, `!=`, `&lt;&gt;`, `ne` respectively. This parameter
     *                         is case-sensitive, values should be lowercase.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function versionCompare(string $version1, string $version2, string $operator = 'eq', string $message = '', mixed ...$args) : void
    {
        $args[] = $version1;
        $args[] = $version2;
        $args[] = $operator;

        static::isTrue(version_compare($version1, $version2, $operator), $message, ...$args);
    }
}