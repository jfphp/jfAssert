<?php

namespace jf\assert;

use BackedEnum;
use DateTimeInterface;
use Stringable;
use UnitEnum;

/**
 * Trait used to do validations and throw exceptions if they are not met.
 */
trait TAssert
{
    /**
     * Check if the assertion is met.
     *
     * @param mixed  $assertion Assertion to check.
     * @param string $message   Message of the exception.
     * @param mixed  ...$args   Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function assert(mixed $assertion, string $message = '', mixed ...$args) : void
    {
        if ($assertion === FALSE)
        {
            if (!$message)
            {
                $_fn = NULL;
                foreach (debug_backtrace(0, 6) as $_trace)
                {
                    if (str_starts_with($_trace['class'] ?? '', __NAMESPACE__))
                    {
                        $_fn = $_trace['function'];
                    }
                    else
                    {
                        break;
                    }
                }
                $message = static::getMessageForFunction($_fn ?: static::classname());
            }

            throw new static(static::formatMessage($message, ...$args));
        }
    }

    /**
     * Returns class name without namespace.
     *
     * @return string
     */
    abstract public static function classname() : string;

    /**
     * Check if value is empty.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function empty(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isTrue(empty($value), $message, ...$args);
    }

    /**
     * Formats the message to return.
     *
     * @param string $message Message to format.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return string
     */
    public static function formatMessage(string $message, mixed ...$args) : string
    {
        if ($message && $args && str_contains($message, '{'))
        {
            $_i  = 0;
            $_tr = [];
            foreach ($args as $_arg)
            {
                $_type                    = gettype($_arg);
                $_tr[ '{' . $_i++ . '}' ] = match (TRUE)
                {
                    $_arg === NULL   => 'NULL',
                    $_arg === FALSE  => 'FALSE',
                    $_arg === TRUE   => 'TRUE',
                    is_scalar($_arg) => $_arg,
                    default          => match (TRUE)
                    {
                        $_arg instanceof DateTimeInterface => $_arg->format('c'),
                        $_arg instanceof BackedEnum        => $_arg->value,
                        $_arg instanceof UnitEnum          => $_arg->name,
                        $_arg instanceof Stringable        => $_arg->__toString(),
                        is_object($_arg)                   => sprintf('[Class %s]', $_arg::class),
                        default                            => json_encode($_arg)
                    }
                };
                $_tr[ '{' . $_i++ . '}' ] = match ($_type)
                {
                    'array'    => sprintf('array<%s>', count($_arg)),
                    'boolean'  => $_arg ? 'TRUE' : 'FALSE',
                    'double'   => 'float',
                    'object'   => $_arg::class,
                    'resource' => sprintf('Resource[%s#%s]', get_resource_type($_arg), get_resource_id($_arg)),
                    default    => $_type
                };
            }
            $message = strtr($message, $_tr);
        }

        return $message;
    }

    /**
     * Translates the error message of the function that threw the exception.
     *
     * @param string $fn Name of the function.
     *
     * @return string
     */
    abstract public static function getMessageForFunction(string $fn) : string;

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::assert($value === FALSE, $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::assert($value === TRUE, $message, ...$args);
    }

    /**
     * Check if value is not empty.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function notEmpty(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::isFalse(empty($value), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is not `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function notIsFalse(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::assert($value !== FALSE, $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is not `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function notIsTrue(mixed $value, string $message = '', mixed ...$args) : void
    {
        $args[] = $value;

        static::assert($value !== TRUE, $message, ...$args);
    }
}