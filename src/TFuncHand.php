<?php

namespace jf\assert;

/**
 * Trait for assertions using functions from PHP module `funchand`.
 *
 * @see https://www.php.net/manual/en/book.funchand.php
 */
trait TFuncHand
{
    /**
     * Return true if the given function has been defined.
     *
     * @param string $function The function name, as a string.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    public static function functionExists(string $function, string $message = '', mixed ...$args) : void
    {
        $args[] = $function;

        static::isTrue(function_exists($function), $message, ...$args);
    }

    /**
     * Verify that the contents of a variable is `FALSE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isFalse(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Verify that the contents of a variable is `TRUE`.
     *
     * @param mixed  $value   Value to check.
     * @param string $message Message of the exception.
     * @param mixed  ...$args Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     */
    abstract public static function isTrue(mixed $value, string $message = '', mixed ...$args) : void;

    /**
     * Evaluates the inverse condition of the method `static::functionExists`.
     *
     * @param string $function The function name, as a string.
     * @param string $message  Message of the exception.
     * @param mixed  ...$args  Placeholders to render message if needed.
     *
     * @return void
     *
     * @throws static
     *
     * @see static::functionExists()
     */
    public static function notFunctionExists(string $function, string $message = '', mixed ...$args) : void
    {
        $args[] = $function;

        static::isFalse(function_exists($function), $message, ...$args);
    }
}