# jf/assert

PHP Assertions library.

## Installation

### Composer

This project uses as dependency manager [Composer](https://getcomposer.org) which can be installed following the
instructions specified in the official [documentation](https://getcomposer.org/doc/00-intro.md) of the project.

To install the `jf/assert` package using this package manager, run:

```sh
composer require jf/assert
```

### Version control

This project can be installed using `git`. First you must clone the project and then install the dependencies:

```sh
git clone https://www.gitlab.com/jfphp/jfAssert.git
cd jfAssert
composer install
```

## Files

### Classes

| Name                                                                                       | Description                                                                                                                                                                                                                                                       |
|:-------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [jf\assert\Assert](src/Assert.php)                                                         | Class used to do validations and throw exceptions if they are not met.                                                                                                                                                                                            |
| [jf\assert\dict\Messages](src/dict/Messages.php)                                           | Class for handling library messages.                                                                                                                                                                                                                              |
| [jf\assert\http\AHttp](src/http/AHttp.php)                                                 | Class base for HTTP exceptions.                                                                                                                                                                                                                                   |
| [jf\assert\http\BadGateway](src/http/BadGateway.php)                                       | Assertions that return the HTTP status code `502` (`Bad Gateway`).                                                                                                                                                                                                |
| [jf\assert\http\BadRequest](src/http/BadRequest.php)                                       | Assertions that return the HTTP status code `400` (`Bad Request`).                                                                                                                                                                                                |
| [jf\assert\http\Conflict](src/http/Conflict.php)                                           | Assertions that return the HTTP status code `409` (`Conflict`).                                                                                                                                                                                                   |
| [jf\assert\http\ExpectationFailed](src/http/ExpectationFailed.php)                         | Assertions that return the HTTP status code `417` (`Expectation Failed`).                                                                                                                                                                                         |
| [jf\assert\http\FailedDependency](src/http/FailedDependency.php)                           | Assertions that return the HTTP status code `424` (`Failed Dependency`).                                                                                                                                                                                          |
| [jf\assert\http\Forbidden](src/http/Forbidden.php)                                         | Assertions that return the HTTP status code `403` (`Forbidden`).                                                                                                                                                                                                  |
| [jf\assert\http\Found](src/http/Found.php)                                                 | Assertions that return the HTTP status code `302` (`Found`).                                                                                                                                                                                                      |
| [jf\assert\http\GatewayTimeout](src/http/GatewayTimeout.php)                               | Assertions that return the HTTP status code `504` (`Gateway Timeout`).                                                                                                                                                                                            |
| [jf\assert\http\Gone](src/http/Gone.php)                                                   | Assertions that return the HTTP status code `410` (`Gone`).                                                                                                                                                                                                       |
| [jf\assert\http\ImATeapot](src/http/ImATeapot.php)                                         | Assertions that return the HTTP status code `418` (`Im A Teapot`).                                                                                                                                                                                                |
| [jf\assert\http\InsufficientStorage](src/http/InsufficientStorage.php)                     | Assertions that return the HTTP status code `507` (`Insufficient Storage`).                                                                                                                                                                                       |
| [jf\assert\http\InternalServerError](src/http/InternalServerError.php)                     | Assertions that return the HTTP status code `500` (`Internal Server Error`).                                                                                                                                                                                      |
| [jf\assert\http\LengthRequired](src/http/LengthRequired.php)                               | Assertions that return the HTTP status code `411` (`Length Required`).                                                                                                                                                                                            |
| [jf\assert\http\Locked](src/http/Locked.php)                                               | Assertions that return the HTTP status code `423` (`Locked`).                                                                                                                                                                                                     |
| [jf\assert\http\LoopDetected](src/http/LoopDetected.php)                                   | Assertions that return the HTTP status code `508` (`Loop Detected`).                                                                                                                                                                                              |
| [jf\assert\http\MethodNotAllowed](src/http/MethodNotAllowed.php)                           | Assertions that return the HTTP status code `405` (`Method Not Allowed`).                                                                                                                                                                                         |
| [jf\assert\http\MisdirectedRequest](src/http/MisdirectedRequest.php)                       | Assertions that return the HTTP status code `421` (`Misdirected Request`).                                                                                                                                                                                        |
| [jf\assert\http\MovedPermanently](src/http/MovedPermanently.php)                           | Assertions that return the HTTP status code `301` (`Moved Permanently`).                                                                                                                                                                                          |
| [jf\assert\http\MultipleChoices](src/http/MultipleChoices.php)                             | Assertions that return the HTTP status code `300` (`Multiple Choices`).                                                                                                                                                                                           |
| [jf\assert\http\NetworkAuthenticationRequired](src/http/NetworkAuthenticationRequired.php) | Assertions that return the HTTP status code `511` (`Network Authentication Required`).                                                                                                                                                                            |
| [jf\assert\http\NotAcceptable](src/http/NotAcceptable.php)                                 | Assertions that return the HTTP status code `406` (`Not Acceptable`).                                                                                                                                                                                             |
| [jf\assert\http\NotExtended](src/http/NotExtended.php)                                     | Assertions that return the HTTP status code `510` (`Not Extended`).                                                                                                                                                                                               |
| [jf\assert\http\NotFound](src/http/NotFound.php)                                           | Assertions that return the HTTP status code `404` (`Not Found`).                                                                                                                                                                                                  |
| [jf\assert\http\NotImplemented](src/http/NotImplemented.php)                               | Assertions that return the HTTP status code `501` (`Not Implemented`).                                                                                                                                                                                            |
| [jf\assert\http\NotModified](src/http/NotModified.php)                                     | Assertions that return the HTTP status code `304` (`Not Modified`).                                                                                                                                                                                               |
| [jf\assert\http\PayloadTooLarge](src/http/PayloadTooLarge.php)                             | Assertions that return the HTTP status code `413` (`Payload Too Large`).                                                                                                                                                                                          |
| [jf\assert\http\PaymentRequired](src/http/PaymentRequired.php)                             | Assertions that return the HTTP status code `402` (`Payment Required`).                                                                                                                                                                                           |
| [jf\assert\http\PermanentRedirect](src/http/PermanentRedirect.php)                         | Assertions that return the HTTP status code `308` (`Permanent Redirect`).                                                                                                                                                                                         |
| [jf\assert\http\PreconditionFailed](src/http/PreconditionFailed.php)                       | Assertions that return the HTTP status code `412` (`Precondition Failed`).                                                                                                                                                                                        |
| [jf\assert\http\PreconditionRequired](src/http/PreconditionRequired.php)                   | Assertions that return the HTTP status code `428` (`Precondition Required`).                                                                                                                                                                                      |
| [jf\assert\http\ProxyAuthenticationRequired](src/http/ProxyAuthenticationRequired.php)     | Assertions that return the HTTP status code `407` (`Proxy Authentication Required`).                                                                                                                                                                              |
| [jf\assert\http\RangeNotSatisfiable](src/http/RangeNotSatisfiable.php)                     | Assertions that return the HTTP status code `416` (`Range Not Satisfiable`).                                                                                                                                                                                      |
| [jf\assert\http\RequestHeaderFieldsTooLarge](src/http/RequestHeaderFieldsTooLarge.php)     | Assertions that return the HTTP status code `431` (`Request Header Fields Too Large`).                                                                                                                                                                            |
| [jf\assert\http\RequestTimeout](src/http/RequestTimeout.php)                               | Assertions that return the HTTP status code `408` (`Request Timeout`).                                                                                                                                                                                            |
| [jf\assert\http\Reserved](src/http/Reserved.php)                                           | Assertions that return the HTTP status code `306` (`Reserved`).                                                                                                                                                                                                   |
| [jf\assert\http\SeeOther](src/http/SeeOther.php)                                           | Assertions that return the HTTP status code `303` (`See Other`).                                                                                                                                                                                                  |
| [jf\assert\http\ServiceUnavailable](src/http/ServiceUnavailable.php)                       | Assertions that return the HTTP status code `503` (`Service Unavailable`).                                                                                                                                                                                        |
| [jf\assert\http\TemporaryRedirect](src/http/TemporaryRedirect.php)                         | Assertions that return the HTTP status code `307` (`Temporary Redirect`).                                                                                                                                                                                         |
| [jf\assert\http\TooEarly](src/http/TooEarly.php)                                           | Assertions that return the HTTP status code `425` (`Too Early`).                                                                                                                                                                                                  |
| [jf\assert\http\TooManyRequests](src/http/TooManyRequests.php)                             | Assertions that return the HTTP status code `429` (`Too Many Requests`).                                                                                                                                                                                          |
| [jf\assert\http\Unauthorized](src/http/Unauthorized.php)                                   | Assertions that return the HTTP status code `401` (`Unauthorized`).                                                                                                                                                                                               |
| [jf\assert\http\UnavailableForLegalReasons](src/http/UnavailableForLegalReasons.php)       | Assertions that return the HTTP status code `451` (`Unavailable For Legal Reasons`).                                                                                                                                                                              |
| [jf\assert\http\UnprocessableEntity](src/http/UnprocessableEntity.php)                     | Assertions that return the HTTP status code `422` (`Unprocessable Entity`).                                                                                                                                                                                       |
| [jf\assert\http\UnsupportedMediaType](src/http/UnsupportedMediaType.php)                   | Assertions that return the HTTP status code `415` (`Unsupported Media Type`).                                                                                                                                                                                     |
| [jf\assert\http\UpgradeRequired](src/http/UpgradeRequired.php)                             | Assertions that return the HTTP status code `426` (`Upgrade Required`).                                                                                                                                                                                           |
| [jf\assert\http\UriTooLong](src/http/UriTooLong.php)                                       | Assertions that return the HTTP status code `414` (`Uri Too Long`).                                                                                                                                                                                               |
| [jf\assert\http\UseProxy](src/http/UseProxy.php)                                           | Assertions that return the HTTP status code `305` (`Use Proxy`).                                                                                                                                                                                                  |
| [jf\assert\http\VariantAlsoNegotiates](src/http/VariantAlsoNegotiates.php)                 | Assertions that return the HTTP status code `506` (`Variant Also Negotiates`).                                                                                                                                                                                    |
| [jf\assert\http\VersionNotSupported](src/http/VersionNotSupported.php)                     | Assertions that return the HTTP status code `505` (`Version Not Supported`).                                                                                                                                                                                      |
| [jf\assert\php\ArgumentCountError](src/php/ArgumentCountError.php)                         | ArgumentCountError is thrown when too few arguments are passed to a user-defined function or method.                                                                                                                                                              |
| [jf\assert\php\ArithmeticError](src/php/ArithmeticError.php)                               | ArithmeticError is thrown when an error occurs while performing mathematical operations. These errors include attempting to perform a bitshift by a negative amount, and any call to intdiv() that would result in a value outside the possible bounds of an int. |
| [jf\assert\php\AssertionError](src/php/AssertionError.php)                                 | AssertionError is thrown when an assertion made via assert() fails.                                                                                                                                                                                               |
| [jf\assert\php\BadFunctionCallException](src/php/BadFunctionCallException.php)             | Exception thrown if a callback refers to an undefined function or if some arguments are missing.                                                                                                                                                                  |
| [jf\assert\php\BadMethodCallException](src/php/BadMethodCallException.php)                 | Exception thrown if a callback refers to an undefined method or if some arguments are missing.                                                                                                                                                                    |
| [jf\assert\php\CompileError](src/php/CompileError.php)                                     | CompileError is thrown for some compilation errors, which formerly issued a fatal error.                                                                                                                                                                          |
| [jf\assert\php\DivisionByZeroError](src/php/DivisionByZeroError.php)                       | DivisionByZeroError is thrown when an attempt is made to divide a number by zero.                                                                                                                                                                                 |
| [jf\assert\php\DomainException](src/php/DomainException.php)                               | Exception thrown if a value does not adhere to a defined valid data domain.                                                                                                                                                                                       |
| [jf\assert\php\Error](src/php/Error.php)                                                   | Error is the base class for all internal PHP errors.                                                                                                                                                                                                              |
| [jf\assert\php\ErrorException](src/php/ErrorException.php)                                 | An Error Exception.                                                                                                                                                                                                                                               |
| [jf\assert\php\Exception](src/php/Exception.php)                                           | Exception is the base class for all user exceptions.                                                                                                                                                                                                              |
| [jf\assert\php\IntlException](src/php/IntlException.php)                                   | This class is used for generating exceptions when errors occur inside intl functions. Such exceptions are only generated when intl.use_exceptions is enabled.                                                                                                     |
| [jf\assert\php\InvalidArgumentException](src/php/InvalidArgumentException.php)             | Exception thrown if an argument is not of the expected type.                                                                                                                                                                                                      |
| [jf\assert\php\JsonException](src/php/JsonException.php)                                   | Exception thrown if JSON_THROW_ON_ERROR option is set for json_encode() or json_decode(). code contains the error type, for possible values see json_last_error().                                                                                                |
| [jf\assert\php\LengthException](src/php/LengthException.php)                               | Exception thrown if a length is invalid.                                                                                                                                                                                                                          |
| [jf\assert\php\LogicException](src/php/LogicException.php)                                 | Exception that represents error in the program logic. This kind of exception should lead directly to a fix in your code.                                                                                                                                          |
| [jf\assert\php\OutOfBoundsException](src/php/OutOfBoundsException.php)                     | Exception thrown if a value is not a valid key. This represents errors that cannot be detected at compile time.                                                                                                                                                   |
| [jf\assert\php\OutOfRangeException](src/php/OutOfRangeException.php)                       | Exception thrown when an illegal index was requested. This represents errors that should be detected at compile time.                                                                                                                                             |
| [jf\assert\php\OverflowException](src/php/OverflowException.php)                           | Exception thrown when adding an element to a full container.                                                                                                                                                                                                      |
| [jf\assert\php\PDOException](src/php/PDOException.php)                                     | Represents an error raised by PDO. You should not throw a PDOException from your own code. See Exceptions for more information about Exceptions in PHP.                                                                                                           |
| [jf\assert\php\ParseError](src/php/ParseError.php)                                         | ParseError is thrown when an error occurs while parsing PHP code, such as when eval() is called.                                                                                                                                                                  |
| [jf\assert\php\PharException](src/php/PharException.php)                                   | The PharException class provides a phar-specific exception class for try/catch blocks.                                                                                                                                                                            |
| [jf\assert\php\RangeException](src/php/RangeException.php)                                 | Exception thrown to indicate range errors during program execution. Normally this means there was an arithmetic error other than under/overflow. This is the runtime version of DomainException.                                                                  |
| [jf\assert\php\ReflectionException](src/php/ReflectionException.php)                       | The ReflectionException class.                                                                                                                                                                                                                                    |
| [jf\assert\php\RuntimeException](src/php/RuntimeException.php)                             | Exception thrown if an error which can only be found on runtime occurs.                                                                                                                                                                                           |
| [jf\assert\php\SodiumException](src/php/SodiumException.php)                               | Exceptions thrown by the sodium functions.                                                                                                                                                                                                                        |
| [jf\assert\php\TypeError](src/php/TypeError.php)                                           | A TypeError may be thrown when.                                                                                                                                                                                                                                   |
| [jf\assert\php\UnderflowException](src/php/UnderflowException.php)                         | Exception thrown when performing an invalid operation on an empty container, such as removing an element.                                                                                                                                                         |
| [jf\assert\php\UnexpectedValueException](src/php/UnexpectedValueException.php)             | Exception thrown if a value does not match with a set of values. Typically this happens when a function calls another function and expects the return value to be of a certain type or value not including arithmetic or buffer related errors.                   |
| [jf\assert\php\UnhandledMatchError](src/php/UnhandledMatchError.php)                       | An UnhandledMatchError is thrown when the subject passed to a match expression is not handled by any arm of the match expression.                                                                                                                                 |
| [jf\assert\php\ValueError](src/php/ValueError.php)                                         | A ValueError is thrown when the type of an argument is correct but the value of it is incorrect. For example, passing a negative integer when the function expects a positive one, or passing an empty string/array when the function expects it to not be empty. |

### Traits

| Name                                         | Description                                                            |
|:---------------------------------------------|:-----------------------------------------------------------------------|
| [jf\assert\TAll](src/TAll.php)               | Trait that includes all traits in the package.                         |
| [jf\assert\TArray](src/TArray.php)           | Trait for assertions using functions from PHP module `array`.          |
| [jf\assert\TAssert](src/TAssert.php)         | Trait used to do validations and throw exceptions if they are not met. |
| [jf\assert\TClassObj](src/TClassObj.php)     | Trait for assertions using functions from PHP module `classobj`.       |
| [jf\assert\TCtype](src/TCtype.php)           | Trait for assertions using functions from PHP module `ctype`.          |
| [jf\assert\TFileSystem](src/TFileSystem.php) | Trait for assertions using functions from PHP module `filesystem`.     |
| [jf\assert\TFilter](src/TFilter.php)         | Trait for assertions using functions from PHP module `filter`.         |
| [jf\assert\TFloat](src/TFloat.php)           | Trait for comparisons between floats.                                  |
| [jf\assert\TFuncHand](src/TFuncHand.php)     | Trait for assertions using functions from PHP module `funchand`.       |
| [jf\assert\TInfo](src/TInfo.php)             | Trait for assertions using functions from PHP module `info`.           |
| [jf\assert\TInteger](src/TInteger.php)       | Trait for comparisons between integers.                                |
| [jf\assert\TLogical](src/TLogical.php)       | Trait for logical comparisons between mixed types.                     |
| [jf\assert\TMath](src/TMath.php)             | Trait for assertions using functions from PHP module `math`.           |
| [jf\assert\TMessage](src/TMessage.php)       | Trait for building the error messages.                                 |
| [jf\assert\TStrings](src/TStrings.php)       | Trait for assertions using functions from PHP module `strings`.        |
| [jf\assert\TVar](src/TVar.php)               | Trait for assertions using functions from PHP module `var`.            |

## Assertions

### jf\assert\TArray

Trait for assertions using functions from PHP module `array`, see [docs](https://www.php.net/manual/en/book.array.php).

| Method                      | Description                                                                                                 |
|:----------------------------|:------------------------------------------------------------------------------------------------------------|
| arrayCountBetweenAnd        | Check that the number of elements in an array or in a Countable object is in the given range.               |
| arrayCountGreaterOrEqual    | Check that the number of elements in an array or in a Countable object is greater than or equal to another. |
| arrayCountGreaterThan       | Check that the number of elements in an array or in a Countable object is greater than another.             |
| arrayCountIdentical         | Check that the number of elements in an array or in a Countable object is identical (`===`) to another.     |
| arrayCountLessOrEqual       | Check that the number of elements in an array or in a Countable object is less than or equal to another.    |
| arrayCountLessThan          | Check that the number of elements in an array or in a Countable object is less than another.                |
| arrayIsList                 | Checks whether a given array is a list.                                                                     |
| arrayKeyExists              | Checks if the given key or index exists in the array.                                                       |
| inArray                     | Checks if a value exists in an array.                                                                       |
| notArrayCountBetweenAnd     | Evaluates the inverse condition of the method `static::arrayCountBetweenAnd`.                               |
| notArrayCountGreaterOrEqual | Evaluates the inverse condition of the method `static::arrayCountGreaterOrEqual`.                           |
| notArrayCountGreaterThan    | Evaluates the inverse condition of the method `static::arrayCountGreaterThan`.                              |
| notArrayCountIdentical      | Evaluates the inverse condition of the method `static::arrayCountIdentical`.                                |
| notArrayCountLessOrEqual    | Evaluates the inverse condition of the method `static::arrayCountLessOrEqual`.                              |
| notArrayCountLessThan       | Evaluates the inverse condition of the method `static::arrayCountLessThan`.                                 |
| notArrayIsList              | Evaluates the inverse condition of the method `static::arrayIsList`.                                        |
| notArrayKeyExists           | Evaluates the inverse condition of the method `static::arrayKeyExists`.                                     |
| notInArray                  | Evaluates the inverse condition of the method `static::inArray`.                                            |

### jf\assert\TAssert

Trait used to do validations and throw exceptions if they are not met.

| Method     | Description                                            |
|:-----------|:-------------------------------------------------------|
| assert     | Check if the assertion is met.                         |
| empty      | Check if value is empty.                               |
| isFalse    | Verify that the contents of a variable is `FALSE`.     |
| isTrue     | Verify that the contents of a variable is `TRUE`.      |
| notEmpty   | Check if value is not empty.                           |
| notIsFalse | Verify that the contents of a variable is not `FALSE`. |
| notIsTrue  | Verify that the contents of a variable is not `TRUE`.  |

### jf\assert\TClassObj

Trait for assertions using functions from PHP module `classobj`, see
[docs](https://www.php.net/manual/en/book.classobj.php).

| Method             | Description                                                                    |
|:-------------------|:-------------------------------------------------------------------------------|
| classAlias         | Creates an alias for a class.                                                  |
| classExists        | Checks if the class has been defined.                                          |
| enumExists         | Checks if the enum has been defined.                                           |
| interfaceExists    | Checks if the interface has been defined.                                      |
| isA                | Checks if the object is of this class or has this class as one of its parents. |
| isSubclassOf       | Checks if the object has this class as one of its parents or implements it.    |
| methodExists       | Checks if the class method exists.                                             |
| notClassAlias      | Evaluates the inverse condition of the method `static::classAlias`.            |
| notClassExists     | Evaluates the inverse condition of the method `static::classExists`.           |
| notEnumExists      | Evaluates the inverse condition of the method `static::enumExists`.            |
| notInterfaceExists | Evaluates the inverse condition of the method `static::interfaceExists`.       |
| notIsA             | Evaluates the inverse condition of the method `static::isA`.                   |
| notIsSubclassOf    | Evaluates the inverse condition of the method `static::isSubclassOf`.          |
| notMethodExists    | Evaluates the inverse condition of the method `static::methodExists`.          |
| notPropertyExists  | Evaluates the inverse condition of the method `static::propertyExists`.        |
| notTraitExists     | Evaluates the inverse condition of the method `static::traitExists`.           |
| propertyExists     | Checks if the object or class has a property.                                  |
| traitExists        | Checks if the trait exists.                                                    |

### jf\assert\TCtype

Trait for assertions using functions from PHP module `ctype`, see [docs](https://www.php.net/manual/en/book.ctype.php).

| Method      | Description                                                                             |
|:------------|:----------------------------------------------------------------------------------------|
| isAlnum     | Check for alphanumeric character(s).                                                    |
| isAlpha     | Check for alphabetic character(s).                                                      |
| isCntrl     | Check for control character(s).                                                         |
| isDigit     | Check for numeric character(s).                                                         |
| isGraph     | Check for any printable character(s) except space.                                      |
| isLower     | Check for lowercase character(s).                                                       |
| isPrint     | Check for printable character(s).                                                       |
| isPunct     | Check for any printable character which is not whitespace or an alphanumeric character. |
| isSpace     | Check for whitespace character(s).                                                      |
| isUpper     | Check for uppercase character(s).                                                       |
| isXdigit    | Check for character(s) representing a hexadecimal digit.                                |
| notIsAlnum  | Evaluates the inverse condition of the method `static::isAlnum`.                        |
| notIsAlpha  | Evaluates the inverse condition of the method `static::isAlpha`.                        |
| notIsCntrl  | Evaluates the inverse condition of the method `static::isCntrl`.                        |
| notIsDigit  | Evaluates the inverse condition of the method `static::isDigit`.                        |
| notIsGraph  | Evaluates the inverse condition of the method `static::isGraph`.                        |
| notIsLower  | Evaluates the inverse condition of the method `static::isLower`.                        |
| notIsPrint  | Evaluates the inverse condition of the method `static::isPrint`.                        |
| notIsPunct  | Evaluates the inverse condition of the method `static::isPunct`.                        |
| notIsSpace  | Evaluates the inverse condition of the method `static::isSpace`.                        |
| notIsUpper  | Evaluates the inverse condition of the method `static::isUpper`.                        |
| notIsXdigit | Evaluates the inverse condition of the method `static::isXdigit`.                       |

### jf\assert\TFileSystem

Trait for assertions using functions from PHP module `filesystem`, see
[docs](https://www.php.net/manual/en/book.filesystem.php).

| Method              | Description                                                               |
|:--------------------|:--------------------------------------------------------------------------|
| chgrp               | Changes file group.                                                       |
| chmod               | Changes file mode.                                                        |
| chown               | Changes file owner.                                                       |
| copy                | Copies file.                                                              |
| fclose              | Closes an open file pointer.                                              |
| fdatasync           | Synchronizes data (but not meta-data) to the file.                        |
| feof                | Tests for end-of-file on a file pointer.                                  |
| fflush              | Flushes the output to a file.                                             |
| fileExists          | Checks whether a file or directory exists.                                |
| flock               | Portable advisory file locking.                                           |
| fnmatch             | Match filename against a pattern.                                         |
| fsync               | Synchronizes changes to the file (including meta-data).                   |
| ftruncate           | Truncates a file to a given length.                                       |
| isDir               | Tells whether the filename is a directory.                                |
| isExecutable        | Tells whether the filename is executable.                                 |
| isFile              | Tells whether the filename is a regular file.                             |
| isLink              | Tells whether the filename is a symbolic link.                            |
| isReadable          | Tells whether a file exists and is readable.                              |
| isUploadedFile      | Tells whether the file was uploaded via HTTP POST.                        |
| isWritable          | Tells whether the filename is writable.                                   |
| lchgrp              | Changes group ownership of symlink.                                       |
| lchown              | Changes user ownership of symlink.                                        |
| link                | Create a hard link.                                                       |
| mkdir               | Makes directory.                                                          |
| moveUploadedFile    | Moves an uploaded file to a new location.                                 |
| notChgrp            | Evaluates the inverse condition of the method `static::chgrp`.            |
| notChmod            | Evaluates the inverse condition of the method `static::chmod`.            |
| notChown            | Evaluates the inverse condition of the method `static::chown`.            |
| notCopy             | Evaluates the inverse condition of the method `static::copy`.             |
| notFclose           | Evaluates the inverse condition of the method `static::fclose`.           |
| notFdatasync        | Evaluates the inverse condition of the method `static::fdatasync`.        |
| notFeof             | Evaluates the inverse condition of the method `static::feof`.             |
| notFflush           | Evaluates the inverse condition of the method `static::fflush`.           |
| notFileExists       | Evaluates the inverse condition of the method `static::fileExists`.       |
| notFlock            | Evaluates the inverse condition of the method `static::flock`.            |
| notFnmatch          | Evaluates the inverse condition of the method `static::fnmatch`.          |
| notFsync            | Evaluates the inverse condition of the method `static::fsync`.            |
| notFtruncate        | Evaluates the inverse condition of the method `static::ftruncate`.        |
| notIsDir            | Evaluates the inverse condition of the method `static::isDir`.            |
| notIsExecutable     | Evaluates the inverse condition of the method `static::isExecutable`.     |
| notIsFile           | Evaluates the inverse condition of the method `static::isFile`.           |
| notIsLink           | Evaluates the inverse condition of the method `static::isLink`.           |
| notIsReadable       | Evaluates the inverse condition of the method `static::isReadable`.       |
| notIsUploadedFile   | Evaluates the inverse condition of the method `static::isUploadedFile`.   |
| notIsWritable       | Evaluates the inverse condition of the method `static::isWritable`.       |
| notLchgrp           | Evaluates the inverse condition of the method `static::lchgrp`.           |
| notLchown           | Evaluates the inverse condition of the method `static::lchown`.           |
| notLink             | Evaluates the inverse condition of the method `static::link`.             |
| notMkdir            | Evaluates the inverse condition of the method `static::mkdir`.            |
| notMoveUploadedFile | Evaluates the inverse condition of the method `static::moveUploadedFile`. |
| notRename           | Evaluates the inverse condition of the method `static::rename`.           |
| notRewind           | Evaluates the inverse condition of the method `static::rewind`.           |
| notRmdir            | Evaluates the inverse condition of the method `static::rmdir`.            |
| notSymlink          | Evaluates the inverse condition of the method `static::symlink`.          |
| notTouch            | Evaluates the inverse condition of the method `static::touch`.            |
| notUnlink           | Evaluates the inverse condition of the method `static::unlink`.           |
| rename              | Renames a file or directory.                                              |
| rewind              | Rewind the position of a file pointer.                                    |
| rmdir               | Removes directory.                                                        |
| symlink             | Creates a symbolic link.                                                  |
| touch               | Sets access and modification time of file.                                |
| unlink              | Deletes a file.                                                           |

### jf\assert\TFilter

Trait for assertions using functions from PHP module `filter`, see
[docs](https://www.php.net/manual/en/book.filter.php).

| Method                  | Description                                                                                                  |
|:------------------------|:-------------------------------------------------------------------------------------------------------------|
| filterHasVar            | Checks if variable of specified type exists.                                                                 |
| filterValidateBool      | Returns true for `1`, `true`, `on` and `yes`. Returns false otherwise.                                       |
| filterValidateDomain    | Validates whether the domain name label lengths are valid.                                                   |
| filterValidateEmail     | Validates whether the value is a valid e-mail address.                                                       |
| filterValidateFloat     | Validates value as float, optionally from the specified range, and converts to float on success.             |
| filterValidateInt       | Validates value as integer, optionally from the specified range, and converts to int on success.             |
| filterValidateIp        | Validates value as IP address, optionally only IPv4 or IPv6 or not from private or reserved ranges.          |
| filterValidateMac       | Validates value as MAC address.                                                                              |
| filterValidateRegexp    | Validates value against regexp, a Perl-compatible regular expression.                                        |
| filterValidateUrl       | Validates value as URL (according to http://www.faqs.org/rfcs/rfc2396), optionally with required components. |
| filterVar               | Filters a variable with a specified filter.                                                                  |
| notFilterHasVar         | Evaluates the inverse condition of the method `static::filterHasVar`.                                        |
| notFilterValidateBool   | Evaluates the inverse condition of the method `static::filterValidateBool`.                                  |
| notFilterValidateDomain | Evaluates the inverse condition of the method `static::filterValidateDomain`.                                |
| notFilterValidateEmail  | Evaluates the inverse condition of the method `static::filterValidateEmail`.                                 |
| notFilterValidateFloat  | Evaluates the inverse condition of the method `static::filterValidateFloat`.                                 |
| notFilterValidateInt    | Evaluates the inverse condition of the method `static::filterValidateInt`.                                   |
| notFilterValidateIp     | Evaluates the inverse condition of the method `static::filterValidateIp`.                                    |
| notFilterValidateMac    | Evaluates the inverse condition of the method `static::filterValidateMac`.                                   |
| notFilterValidateRegexp | Evaluates the inverse condition of the method `static::filterValidateRegexp`.                                |
| notFilterValidateUrl    | Evaluates the inverse condition of the method `static::filterValidateUrl`.                                   |
| notFilterVar            | Evaluates the inverse condition of the method `static::filterVar`.                                           |

### jf\assert\TFloat

Trait for comparisons between floats.

| Method                 | Description                                                                  |
|:-----------------------|:-----------------------------------------------------------------------------|
| floatBetweenAnd        | Check that a value of type float is in the given range.                      |
| floatGreaterOrEqual    | Check that a value of type float is greater than or equal to another.        |
| floatGreaterThan       | Check that a value of type float is greater than another.                    |
| floatIdentical         | Check that a value of type float is identical (`===`) to another.            |
| floatLessOrEqual       | Check that a value of type float is less than or equal to another.           |
| floatLessThan          | Check that a value of type float is less than another.                       |
| notFloatBetweenAnd     | Evaluates the inverse condition of the method `static::floatBetweenAnd`.     |
| notFloatGreaterOrEqual | Evaluates the inverse condition of the method `static::floatGreaterOrEqual`. |
| notFloatGreaterThan    | Evaluates the inverse condition of the method `static::floatGreaterThan`.    |
| notFloatIdentical      | Evaluates the inverse condition of the method `static::floatIdentical`.      |
| notFloatLessOrEqual    | Evaluates the inverse condition of the method `static::floatLessOrEqual`.    |
| notFloatLessThan       | Evaluates the inverse condition of the method `static::floatLessThan`.       |

### jf\assert\TFuncHand

Trait for assertions using functions from PHP module `funchand`, see
[docs](https://www.php.net/manual/en/book.funchand.php).

| Method            | Description                                                             |
|:------------------|:------------------------------------------------------------------------|
| functionExists    | Return true if the given function has been defined.                     |
| notFunctionExists | Evaluates the inverse condition of the method `static::functionExists`. |

### jf\assert\TInfo

Trait for assertions using functions from PHP module `info`, see [docs](https://www.php.net/manual/en/book.info.php).

| Method             | Description                                                              |
|:-------------------|:-------------------------------------------------------------------------|
| extensionLoaded    | Find out whether an extension is loaded.                                 |
| notExtensionLoaded | Evaluates the inverse condition of the method `static::extensionLoaded`. |
| notVersionCompare  | Evaluates the inverse condition of the method `static::versionCompare`.  |
| versionCompare     | Compares two "PHP-standardized" version number strings.                  |

### jf\assert\TInteger

Trait for comparisons between integers.

| Method               | Description                                                                |
|:---------------------|:---------------------------------------------------------------------------|
| intBetweenAnd        | Check that a value of type int is in the given range.                      |
| intGreaterOrEqual    | Check that a value of type int is greater than or equal to another.        |
| intGreaterThan       | Check that a value of type int is greater than another.                    |
| intIdentical         | Check that a value of type int is identical (`===`) to another.            |
| intLessOrEqual       | Check that a value of type int is less than or equal to another.           |
| intLessThan          | Check that a value of type int is less than another.                       |
| notIntBetweenAnd     | Evaluates the inverse condition of the method `static::intBetweenAnd`.     |
| notIntGreaterOrEqual | Evaluates the inverse condition of the method `static::intGreaterOrEqual`. |
| notIntGreaterThan    | Evaluates the inverse condition of the method `static::intGreaterThan`.    |
| notIntIdentical      | Evaluates the inverse condition of the method `static::intIdentical`.      |
| notIntLessOrEqual    | Evaluates the inverse condition of the method `static::intLessOrEqual`.    |
| notIntLessThan       | Evaluates the inverse condition of the method `static::intLessThan`.       |

### jf\assert\TLogical

Trait for logical comparisons between mixed types.

| Method            | Description                                                             |
|:------------------|:------------------------------------------------------------------------|
| betweenAnd        | Check that a value is in the given range.                               |
| equal             | Check that a value is equal (`==`) to another.                          |
| greaterOrEqual    | Check that a value is greater than or equal to another.                 |
| greaterThan       | Check that a value is greater than another.                             |
| identical         | Check that a value is identical (`===`) to another.                     |
| lessOrEqual       | Check that a value is less than or equal to another.                    |
| lessThan          | Check that a value is less than another.                                |
| notBetweenAnd     | Evaluates the inverse condition of the method `static::betweenAnd`.     |
| notEqual          | Evaluates the inverse condition of the method `static::equal`.          |
| notGreaterOrEqual | Evaluates the inverse condition of the method `static::greaterOrEqual`. |
| notGreaterThan    | Evaluates the inverse condition of the method `static::greaterThan`.    |
| notIdentical      | Evaluates the inverse condition of the method `static::identical`.      |
| notLessOrEqual    | Evaluates the inverse condition of the method `static::lessOrEqual`.    |
| notLessThan       | Evaluates the inverse condition of the method `static::lessThan`.       |

### jf\assert\TMath

Trait for assertions using functions from PHP module `math`, see [docs](https://www.php.net/manual/en/book.math.php).

| Method        | Description                                                         |
|:--------------|:--------------------------------------------------------------------|
| isFinite      | Finds whether a value is a legal finite number.                     |
| isInfinite    | Finds whether a value is infinite.                                  |
| isNan         | Finds whether a value is not a number.                              |
| notIsFinite   | Evaluates the inverse condition of the method `static::isFinite`.   |
| notIsInfinite | Evaluates the inverse condition of the method `static::isInfinite`. |
| notIsNan      | Evaluates the inverse condition of the method `static::isNan`.      |

### jf\assert\TStrings

Trait for assertions using functions from PHP module `strings`, see
[docs](https://www.php.net/manual/en/book.strings.php).

| Method                     | Description                                                                      |
|:---------------------------|:---------------------------------------------------------------------------------|
| notRegex                   | Evaluates the inverse condition of the method `static::regex`.                   |
| notStrContains             | Evaluates the inverse condition of the method `static::strContains`.             |
| notStrEndsWith             | Evaluates the inverse condition of the method `static::strEndsWith`.             |
| notStrLengthBetweenAnd     | Evaluates the inverse condition of the method `static::strLengthBetweenAnd`.     |
| notStrLengthGreaterOrEqual | Evaluates the inverse condition of the method `static::strLengthGreaterOrEqual`. |
| notStrLengthGreaterThan    | Evaluates the inverse condition of the method `static::strLengthGreaterThan`.    |
| notStrLengthIdentical      | Evaluates the inverse condition of the method `static::strLengthIdentical`.      |
| notStrLengthLessOrEqual    | Evaluates the inverse condition of the method `static::strLengthLessOrEqual`.    |
| notStrLengthLessThan       | Evaluates the inverse condition of the method `static::strLengthLessThan`.       |
| notStrStartsWith           | Evaluates the inverse condition of the method `static::strStartsWith`.           |
| regex                      | Check that a string matches a regular expression.                                |
| strContains                | Determine if a string contains a given substring.                                |
| strEndsWith                | Checks if a string ends with a given substring.                                  |
| strLengthBetweenAnd        | Check that the length of string is in the given range.                           |
| strLengthGreaterOrEqual    | Check that the length of string is greater than or equal to another.             |
| strLengthGreaterThan       | Check that the length of string is greater than another.                         |
| strLengthIdentical         | Check that the length of string is identical (`===`) to another.                 |
| strLengthLessOrEqual       | Check that the length of string is less than or equal to another.                |
| strLengthLessThan          | Check that the length of string is less than another.                            |
| strStartsWith              | Checks if a string starts with a given substring.                                |

### jf\assert\TVar

Trait for assertions using functions from PHP module `var`, see [docs](https://www.php.net/manual/en/book.var.php).

| Method           | Description                                                             |
|:-----------------|:------------------------------------------------------------------------|
| isArray          | Finds whether a variable is an array.                                   |
| isBool           | Finds out whether a variable is a boolean.                              |
| isCallable       | Verify that a value can be called as a function from the current scope. |
| isCountable      | Verify that the contents of a variable is a countable value.            |
| isFloat          | Finds whether the type of a variable is float.                          |
| isInstanceOf     | Check that a value is an instance of specified class.                   |
| isInt            | Find whether the type of a variable is integer.                         |
| isIterable       | Verify that the contents of a variable is an iterable value.            |
| isNull           | Finds whether a variable is null.                                       |
| isNumeric        | Finds whether a variable is a number or a numeric string.               |
| isObject         | Finds whether a variable is an object.                                  |
| isResource       | Finds whether a variable is a resource.                                 |
| isScalar         | Finds whether a variable is a scalar.                                   |
| isString         | Find whether the type of a variable is string.                          |
| isTraversable    | Check that a value is an array or an instance of `\Traversable`.        |
| notIsArray       | Evaluates the inverse condition of the method `static::isArray`.        |
| notIsBool        | Evaluates the inverse condition of the method `static::isBool`.         |
| notIsCallable    | Evaluates the inverse condition of the method `static::isCallable`.     |
| notIsCountable   | Evaluates the inverse condition of the method `static::isCountable`.    |
| notIsFloat       | Evaluates the inverse condition of the method `static::isFloat`.        |
| notIsInstanceOf  | Evaluates the inverse condition of the method `static::isInstanceOf`.   |
| notIsInt         | Evaluates the inverse condition of the method `static::isInt`.          |
| notIsIterable    | Evaluates the inverse condition of the method `static::isIterable`.     |
| notIsNull        | Evaluates the inverse condition of the method `static::isNull`.         |
| notIsNumeric     | Evaluates the inverse condition of the method `static::isNumeric`.      |
| notIsObject      | Evaluates the inverse condition of the method `static::isObject`.       |
| notIsResource    | Evaluates the inverse condition of the method `static::isResource`.     |
| notIsScalar      | Evaluates the inverse condition of the method `static::isScalar`.       |
| notIsString      | Evaluates the inverse condition of the method `static::isString`.       |
| notIsTraversable | Evaluates the inverse condition of the method `static::isTraversable`.  |
